__author__ = 'matt.lievertz'


# Dependencies: this is python3 and uses non-standard library 'requests'
# Must be logged into OpenVPN at runtime for the environment specified

import requests
import csv
import os
import sys


#Variables you'll want to change

filename = "ExpectedUsers.csv"  # csv to pull expected displayName and email from, will also count entries
expectedAdditionalUsers = 0  # Number of users in purecloud expected in addition to those in csv
domain = 'us-east-1.inintca.com'  # example value is 'us-east-1.inindca.com'

allOrgs = []  # Add each org in a line like this: allOrgs.append('4b113289-9268-41c1-bada-8ff6b590db45')
allOrgs.append('43dc3234-cef7-4007-8b8e-72f872b53ae3')
# Note: just left multi-org plumbing in for future; don't have support for multi-csv yet makes this useless
#       I can add multi-csv support pretty easy if needed


# Global Variables you may wish to alter, but you probably don't need to
retries = 5
timeout = 120  # in Seconds
# wait = 300  # in Seconds


# General functions

# function to send GET calls
# code = desired code with default value 200
def getRequest(url, code=200):
    success = 0  # probably not necessary to declare

    # While loop implements total number of retries for timeout and status code
    tries = 0
    while (tries < retries):
        try:
            # actually send request here
            res = requests.get(url, timeout=timeout)

            if (res.status_code == code):
                # success path
                tries = retries
                success = 1
            else:
                # fail for wrong http status code
                tries += 1
                print("")
                print("Retry needed for GET. Status code: " + str(res.status_code))
                try:
                    print("Correlation Id: " + str(res.headers['ININ-Correlation-Id']))
                except:
                    print("-----No correlation Id-----")
                print("")
        except requests.Timeout:
            # fail for timeout
            tries += 1
            print("Timeout of " + str(timeout) + "s exceeded")
        except:
            # fail for other reasons
            tries += 1
            print("Mysterious issue encountered")
            print(str(res.text))

    return res, success


# Specific functions

#  get to config private api for users asc with given page number
def getUsersConfig(orgId, page):
    getUsersConfigUrl = \
            'http://configuration.{0}' \
            '/configurations/v1/organizations/{1}/users?page={2}&sortOrder=asc' \
            .format(domain, orgId, page)

    userList = []

    # call function to make request here
    res, success = getRequest(getUsersConfigUrl)

    if (success == 1):
        # store totalPages
        totalPages = res.json()['totalPages']

        # store totalPages
        totalUsers = res.json()['totalResults']

        # extract and store userIds
        for a in res.json()['content']:
            userList.append((a['displayName'], a['email'], a['id']))

        #  print(str(userList))
        return userList, totalPages, totalUsers
    else:
        print("Request to get page " + str(page) + " of users in org " + str(orgId) + "failed. Exiting.")
        # Some better way of handling this?

# see if a 2-value tuple exists in a list of tuples checking the first two values in each tuple for ordered match
def compare(name, email, list):

    included = False
    detail = "Program error in compare function!"

    for i in range(0, len(list), 1):
        if (name == list[i][0]):
            if (email == list[i][1]):
                included = True
                detail = "SUCCESS"
            else:
                detail = "Match name present: " + str(name) + " Email " + str(email) + " absent"
        else:
            if (email == list[i][1]):
                detail = "Match email present: " + str(email) + " Name " + str(name) + " absent"
            else:
                detail = "No matches on either - Name: " + str(name) + " Email: " + str(email)

    return included, detail


# Execution

os.chdir(os.path.dirname(os.path.abspath(__file__)))
# print(str(os.getcwd()))

# do this for each org
for i in allOrgs:

    foundTotalUserList = []
    foundTotalUserListAdjusted = []

    print("")
    print("")
    print("     ---------- BEGINNING NEW ORG ----------     ")
    print("")
    print("")

    # get number of pages of users
    userList, totalPages, totalUsers = getUsersConfig(i, '0')
    print("  Org " + str(i))
    print("")
    print("  Total Pages to get: " + str(totalPages))
    print("  Total Users in org: " + str(totalUsers))
    print("")

    # get every page of users and build total found user list
    print("  Now getting pages of users from PureCloud:")
    for j in range(totalPages, 0, -1):
        userList, totalPages, totalUsers = getUsersConfig(i, j)
        print("    Got page " + str(1 + totalPages - j) + " of " + str(totalPages))

        # print(str(userList))
        for m,n,o in userList:
            foundTotalUserList.append((m, n, o))

    # print(str(foundTotalUserList))
    print("  Finished getting users from PureCloud")

    # get list of expected user data
    print("")
    print("  Now getting expected users from csv")
    expectedCSV = open(filename)
    expectedObject = csv.reader(expectedCSV)

    expectedTotalUserList = []

    counter=0  # this used to skip header row
    for row in expectedObject:
        if (counter > 0):
            expectedTotalUserList.append((row[0], row[1]))
        counter += 1

    expectedCSV.close()
    print("  Finished getting expected users from csv")

    # verify number of users in total
    expectedTotalUserCount = len(expectedTotalUserList) + expectedAdditionalUsers

    print("  We expected " + str(expectedTotalUserCount) + " users in the org and found " + str(totalUsers) + " users")

    # Diff the lists
    # Find entries in the expected list that don't appear in the found list
    print("")
    print("  Now identifying Users in the csv that are not present in PureCloud:")
    print("")

    missingUsers = 0
    for a, b in expectedTotalUserList:
        included, detail = compare(a, b, foundTotalUserList)

        if included == False:
            print("INVESTIGATE MISSING in cloud - " + str(detail))
            missingUsers += 1

    print("")
    if missingUsers == 0:
        print("  Finished - all users in csv are present in PureCloud")
    else:
        print("  Finished - found " + str(missingUsers) + " users in csv that weren't in PureCloud")

    # Find entries in the found list that don't appear in the expected list
    print("")
    print("  Now identifying Users in PureCloud that are not present in the csv:")
    print("")

    extraUsers = 0
    for x, y, z in foundTotalUserList:
        included, detail = compare(x, y, expectedTotalUserList)

        if included == False:
            print("INVESTIGATE EXTRA in cloud - " + str(detail))
            extraUsers += 1

    print("")
    if extraUsers == expectedAdditionalUsers:
        print("  Finished - users as expected")
    else:
        print("  Finished - found " + str(extraUsers) + " users in cloud that weren't in the csv")
        print("             (Expected " + str(expectedAdditionalUsers) + " extra users)")

print("")
print("")
print("")