__author__ = 'matt.lievertz'


# Dependencies:
#   1) Python 3.x (download at https://www.python.org/downloads/)
#   2) This uses non-standard library 'requests' (at path, type 'pip3 install requests')
#   3) Must be logged into OpenVPN at runtime
#   4) You need to make sure you change relevant variables at the top:
#           Feature toggles
#           userPrefix, userRangeMin, userRangeMax
#           allOrgs

import requests
# import time


# Feature toggles
skipAllConversationStuff = 0 # value 1 will skip everything but edge reboot
# these four are sub to skipAllConversationStuff
detectConversationsOnly = 0  # value 1 will skip deletes
sendConvDeletesAllUsers = 1 # value 1 will send conversation deletes for all users, not just detected users
sendAssignmentDeletesAllUsers = 1  # value 1 will send assignment deletes for all users, not just detected users
deleteStationAssociations = 1  # value 1 will send

rebootEdges = 1 # value 1 will reboot edges


# Global Variables you may wish to alter

domain = 'us-east-1.inintca.com'  # example value is us-east-1.inintca.com
retries = 5
timeout = 120  # in Seconds
# wait = 300  # in Seconds

# orgDomainPrefix = 'testacdload' - we're dynamically getting this from cloud in getOrgName()
orgDomainTLD = 'test'  # TLD is Top Level Domain e.g., test, com, org, gov
# orgRangeMin = 1 - we're dynamically getting this from cloud in getOrgName()
# orgRangeMax = 12 - we're dynamically getting this from cloud in getOrgName()

# For list of users in User0, User1, ..., UserN format, give range min/max and prefix (e.g., 'User')
userPrefix = 'User'
userRangeMin = 0
userRangeMax = 500

# Declare orgs list. Use list.append() to keep list easy to manage
# You can easily comment out orgs this way too
allOrgs = []
# This is the org in the engage multi-edge validation env in TCA
# allOrgs.append('e04bb3fc-97f0-4fca-b016-e4d466bd1087')
# These are the orgs in the Inbound ACD Load testing environment in TCA
# allOrgs.append('6123f0a6-5fdf-4140-ac13-0e6fa181ad77')
# allOrgs.append('8ff8cfcc-69bc-45cf-a981-f045313a5717')
# allOrgs.append('ae5d0f95-56aa-478d-b413-78d1269904fd')
# allOrgs.append('e9f12fdb-6186-4216-b4f2-e4b5e45f149d')
# allOrgs.append('91a78905-2e25-4d5b-ae86-981fa7b302b2')
# allOrgs.append('d07b2dcd-46ff-4e1d-998c-2e4028eb271a')
# allOrgs.append('435e440a-a5de-469b-8a59-f6174776720d')
# allOrgs.append('2ccfb5e9-e306-4fe1-9d1c-cda8493afbfa')
# allOrgs.append('d98c530e-0e17-49f7-ab50-309db7e4213f')
# allOrgs.append('9105eb71-1b8b-4b01-9a73-8fd04789ff33')
# allOrgs.append('21dc9d3c-0eaa-4495-9d73-407c0aa67072')
# allOrgs.append('b0bbf4ff-b3b4-4c81-9ca2-271f0c76f2bc')
# This is GregS' org in TCA
# allOrgs.append('a87e93aa-b6fe-4200-bec3-5c4223035db3')


# General functions

# function to send GET calls
# code = desired code with default value 200
def getRequest(url, code=200):
    success = 0  # probably not necessary to declare

    # While loop implements total number of retries for timeout and status code
    tries = 0
    while (tries < retries):
        try:
            # actually send request here
            res = requests.get(url, timeout=timeout)

            if (res.status_code == code):
                # success path
                tries = retries
                success = 1
            else:
                # fail for wrong http status code
                tries += 1
                print("")
                print("! Retry needed for GET. Status code: " + str(res.status_code))
                try:
                    print("! Correlation Id: " + str(res.headers['ININ-Correlation-Id']))
                except:
                    print("! -----No correlation Id-----")
                print("")
            break
        except requests.Timeout:
            # fail for timeout
            tries += 1
            print("! imeout of " + str(timeout) + "s exceeded")
        else:
            # fail for other reasons
            tries += 1
            print("Mysterious issue encountered")

    return res, success

# function to send DELETE calls
# code = desired code with default value 200
def deleteRequest(url, code=200):
    success = 0 # probably not necessary to declare

    # While loop implements total number of retries for timeout and status code
    tries = 0
    while (tries < retries):
        try:
            # actually send request here
            res = requests.delete(url, timeout=timeout)

            if (res.status_code == code):
                # success path
                tries = retries
                success = 1
            else:
                # fail for wrong http status code
                tries += 1
                print("")
                print("! Retry needed for DELETE. Status code: " + str(res.status_code))
                print("")
            break
        except requests.Timeout:
            # fail for timeout
            tries += 1
            print("Timeout of " + str(timeout) + "s exceeded")
        else:
            # fail for other reasons
            tries += 1
            print("Mysterious issue encountered")

    return res, success

# function to send POST calls
# code = desired code with default value 200
def postRequest(url, payload, headers=None, params=None, code=200):
    success = 0 # probably not necessary to declare

    # While loop implements total number of retries for timeout and status code
    tries = 0
    while (tries < retries):
        try:
            # actually send request here
            res = requests.post(url, json=payload, headers=headers, params=params, timeout=timeout)

            if (res.status_code == code):
                # success path
                tries = retries
                success = 1
            else:
                # fail for wrong http status code
                tries += 1
                print("")
                print("! Retry needed for POST. Status code: " + str(res.status_code))
                print("")
        except requests.Timeout:
            # fail for timeout
            tries += 1
            print("Timeout of " + str(timeout) + "s exceeded")
        except:
            # fail for other reasons
            tries += 1
            print("Mysterious issue encountered")

    return res, success


# Specific functions

# get to config private api for users asc with given page number
def getUsersConfig(orgId, page):
    getUsersConfigUrl = \
            'http://directory.{0}' \
            '/directory/v1/organizations/{1}/users?number={2}&sortOrder=asc' \
            .format(domain, orgId, page)

    userList = []

    # call function to make request here
    res, success = getRequest(getUsersConfigUrl)

    if (success == 1):
        # store totalPages
        totalPages = res.json()['totalPages']

        # extract and store userIds
        for a in res.json()['content']:
            userList.append((a['id'], a['email']))

        return userList, totalPages
    else:
        print("! Request to get page" + str(page) + "of users in org " + str(orgId) + "failed. Exiting.")
        # Some better way of handling this?

# get to config private api for thirdPartyOrgName of orgId
# assuming here that {thirdPartyOrgName}.TLD is the format of email addresses for users
def getOrgName(orgId):
    getOrgNameConfigUrl = \
            'http://directory.{0}' \
            '/directory/v1/organizations/{1}' \
            .format(domain, orgId)

    # call function to send request
    res, success = getRequest(getOrgNameConfigUrl)

    if (success == 1):
        # extract and return orgName
        orgName = res.json()['thirdPartyOrgName']
        return orgName
    else:
        print("! Request to get org name failed in org " + str(orgId) + ". Exiting.")
        # Some better way of handling this?

# filter userList to just users within defined range
def filterUsers(userList, orgName):
    # put together object for email suffix
    userEmailSuffix = "@" + str(orgName) + "." + str(orgDomainTLD)
    Suffixlength = len(userEmailSuffix)
    # userPrefix is a global variable
    Prefixlength = len(userPrefix)

    filteredUserList = []

    # for each id, email in userList
    # Going to slice prefix and suffix off and test user number within range
    for n, m in userList:
        # need to test for different pattern users like "admin"
        justPrefix = m[0:int(Prefixlength)]  # -1 to adjust to index? apparently not...
        if (justPrefix == userPrefix):

            # isolate number in email
            noSuffix = m[:int(len(m) - Suffixlength)]
            justNumber = noSuffix[int(Prefixlength):]

            try:
                # assuming we have a number here and it is in range, add it to filteredUserList
                if (userRangeMin < int(justNumber) < userRangeMax):
                    filteredUserList.append((n, m, '0'))
            except TypeError:
                print("For some reason the user prefix/suffix was not as expected and")
                print("what was left was not a number.")
                print("Suffixlength: " + str(Suffixlength))
                print("userEmailSuffix: " + str(userEmailSuffix))
                print("noSuffix: " + str(noSuffix))
                print("justNumber: " + str(justNumber))
            except ValueError:
                print("For some reason the user prefix/suffix was not as expected and")
                print("what was left was not a number.")
                print("Suffixlength: " + str(Suffixlength))
                print("userEmailSuffix: " + str(userEmailSuffix))
                print("noSuffix: " + str(noSuffix))
                print("justNumber: " + str(justNumber))
                # Handle this somehow eventually

        else:
            print("! Automatically didn't include - detected prefix pattern non-match: " + str(userPrefix))
            print("                                                       Instead was: " + str(justPrefix))
            print("                                                          For user: " + str(m))
            print("")

    return filteredUserList

# get to conversation private api for conversations on users
def getUserConversations(filteredUserList, orgId):

    finalUserList=[]

    # send a delete for each user in the user list
    for n, m, p in filteredUserList:
        #n is user ID, p is '0' right now, but we will set it to conversation ID if there is one

        getUserConversationsUrl = \
                'http://conversation.{0}' \
                '/conversations/organizations/{1}/users/{2}/conversations' \
                .format(domain, orgId, n)

        # call function to make request here
        res, success = getRequest(getUserConversationsUrl)

        if (success == 1):
            if (len(res.text) >= 36):
                # the conversation guid is 36 characters long
                # print(res.text)
                # print("")
                # Eventually add support for multiple conversations using code like below
                # for a in res.json():
                #    p.append((a['id']))
                p=res.json()[0]['id']
                print("")
                print("! " + str(m) + " - " + str(p))
                print("")
            else:
                print("  " + str(m) + " - No conversations")

            finalUserList.append((n, m, p))
        else:
            print("! Request to get conversations for " + str(m) + " id " + str(n) + "failed")
            # Some better way of handling this?

    return finalUserList

# get Edges in an org
def getEdges(orgId):

    edges = []

    getEdgesConfigUrl = \
            'http://configuration.{0}' \
            '/configurations/v1/organizations/{1}/edges' \
            .format(domain, orgId)

    # call function to send request
    res, success = getRequest(getEdgesConfigUrl)

    if (success == 1):
        # extract and return edge IDs
        for a in res.json()['content']:
            edges.append(a['id'])
        return edges
    else:
        print("! Request to get org edges failed in org " + str(orgId) + ". Exiting.")
        # Some better way of handling this?

# send conversation service delete for each user id in list
def deleteUserConversations(filteredUserList, orgId):

    # send a delete for each user in the user list
    for n, m, p in filteredUserList:
        # just need to use n which is userId

        if (int(len(p)) > 30) or (sendConvDeletesAllUsers == 1):
            deleteUserConversationsUrl = \
                'http://conversation.{0}' \
                '/conversations/organizations/{1}/users/{2}/conversations' \
                .format(domain, orgId, n)

            res, success = deleteRequest(deleteUserConversationsUrl)

            if (success == 1):
                print("  Delete conversation succeeded for " + str(m) + " conversation: " + str(p))
            else:
                print("")
                print("! Delete conversation failed for " + str(n) + " " + str(m))
                print("")

# send assignment service delete for each user id in list
def deleteUserAssignmentRecords(filteredUserList, orgId):

    for n, m, p in filteredUserList:
        # just need to use n which is userId
        # send a delete for each user in the user list

        if (int(len(p)) > 30) or (sendAssignmentDeletesAllUsers == 1):
            deleteUserAssignmentUrl = \
                'http://assignment.{0}' \
                '/assignment/organizations/{1}/agents/{2}' \
                .format(domain, orgId, n)

            res, success = deleteRequest(deleteUserAssignmentUrl)

            if (success == 1):
                print("  Delete assignment record succeeded for " + str(m) + " conversation: " + str(p))
            else:
                print("! Delete assignment record failed for " + str(n) + " " + str(m))

# send a delete for each user to clear station associations
def deleteUserStationAssociation(filteredUserList, orgId):

    for n, m, p in filteredUserList:
        # just need to use n which is userId
        # send a delete for each user in the user list
        deleteUserStationAssociationUrl = \
            'http://user.{0}' \
            '/user/v2/organizations/{1}/users/{2}/associatedStation' \
            .format(domain, orgId, n)

        res, success = deleteRequest(deleteUserStationAssociationUrl)

        if (success == 1):
            # print("  " + str(m) + " - association delete succeeded")
            pass
        else:
            print("! Delete user station association failed for " + str(n) + " " + str(m))

# send a post to reboot an edge
def rebootEdge(edgeId, orgId):
    postEdgeRebootUrl = \
            'http://update.{0}' \
            '/update/v1/edges/{1}/reboot' \
            .format(domain, edgeId)

    header = {'ININ-Organization-Id': orgId}

    res, success = postRequest(postEdgeRebootUrl,"", header)

    if (success == 1):
        print("  Reboot edge succeeded for " + str(edgeId))
    else:
        print("! Reboot edge failed for " + str(edgeId))


# For the future

# def delete conversations in queue():
    # not even sure what the best way to go about this is


# Execution
for i in allOrgs:
    # do this for each org
    print("")
    print("")
    print("     ---------- BEGINNING NEW ORG ----------     ")
    print("")
    print("")

    # get number of pages of users in each org
    userList, totalPages = getUsersConfig(i, '1')
    print("Org " + str(i) + " - Total pages: " + str(totalPages))

    # get orgName
    orgName = getOrgName(i)

    # workaround for Greg's org
    # orgName = "gregssandbox"

    print("Org " + str(i) + " - Name: " + str(orgName))

    if (skipAllConversationStuff != 1):
        for j in range(totalPages, 0, -1):
            # get every page of users
            userList, totalPages = getUsersConfig(i, j)
            print("")
            print("Got page " + str(j) + " in Org " + str(orgName))

            # filter user list to users in range
            filteredUserList = filterUsers(userList, orgName)
            print("  Filter user list for this page completed")

            if (deleteStationAssociations == 1):
                print("")
                print("  Now deleting station associations for all users in range:")
                # delete user station associations
                deleteUserStationAssociation(filteredUserList, i)
                print("  Association deletes for page " + str(j) + " in Org " + str(orgName) + " completed")

            finalUserList = getUserConversations(filteredUserList, i)
            print("")
            print("  User Conversation information added to list for this page")
            print("")

            if (detectConversationsOnly != 1):
                if (sendConvDeletesAllUsers == 1):
                    print("  Now proceeding to delete conversations for all users")
                else:
                    print("  Now proceeding to deletes if any conversations were detected:")
                print("")
                # delete user Conversations
                deleteUserConversations(finalUserList, i)
                print("")
                # delete user Assignment Records
                deleteUserAssignmentRecords(finalUserList, i)
                print("")
                print("  Deletes for page " + str(j) + " in Org " + str(orgName) + " completed")

    if (rebootEdges == 1):
        print("")
        print("  Now getting edges")
        edges = getEdges(i)

        for edge in edges:
            print("")
            rebootEdge(edge, i)


