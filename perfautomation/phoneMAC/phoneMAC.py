__author__ = 'matt.lievertz'


# Dependencies:
#   1) Python 3.x (download at https://www.python.org/downloads/)
#   2) This uses non-standard library 'requests' (at path, type 'pip3 install requests')
#   3) Must be logged into OpenVPN at runtime
#   4) You need to make sure you change relevant variables at the top:
#       specifically the AllOrgs values
#       and domain if relevant


# Modules to import
import requests


# Feature toggles
#   None at this time


# Global Variables you may wish to alter

# Filter phones to update mac for
phoneRangeMin = 10000
phoneRangeMax = 17000

# Mac address range to use:
# Min will be first value; max just gives a warning if exceeded with no enforcement
macRangeMin = 0
macRangeMax = 999999

# These are the important values to change for your run:
#   First value is org Id for your org
#   Second value is the mac prefix (6 digit hex as string, you provide)
#   Third value is the phone label prefix (as exists on the phone in cloud)
#   Note: I use list.append() to keep list easy to manage and allow easy comment-out of orgs
allOrgs = []
# These below are the 12 orgs in the Inbound ACD Load testing environment in TCA
# allOrgs.append(('6123f0a6-5fdf-4140-ac13-0e6fa181ad77', 'acd001', 'load1Phone'))
# allOrgs.append(('8ff8cfcc-69bc-45cf-a981-f045313a5717', 'acd002', 'load2Phone'))
# allOrgs.append(('ae5d0f95-56aa-478d-b413-78d1269904fd', 'acd003', 'load3Phone'))
# allOrgs.append(('e9f12fdb-6186-4216-b4f2-e4b5e45f149d', 'acd004', 'load4Phone'))
# allOrgs.append(('91a78905-2e25-4d5b-ae86-981fa7b302b2', 'acd005', 'load5Phone'))
# allOrgs.append(('d07b2dcd-46ff-4e1d-998c-2e4028eb271a', 'acd006', 'load6Phone'))
# allOrgs.append(('435e440a-a5de-469b-8a59-f6174776720d', 'acd007', 'load7Phone'))
# allOrgs.append(('2ccfb5e9-e306-4fe1-9d1c-cda8493afbfa', 'acd008', 'load8Phone'))
# allOrgs.append(('d98c530e-0e17-49f7-ab50-309db7e4213f', 'acd009', 'load9Phone'))
# allOrgs.append(('9105eb71-1b8b-4b01-9a73-8fd04789ff33', 'acd010', 'load10Phone'))
# allOrgs.append(('21dc9d3c-0eaa-4495-9d73-407c0aa67072', 'acd011', 'load11Phone'))
# allOrgs.append(('b0bbf4ff-b3b4-4c81-9ca2-271f0c76f2bc', 'acd012', 'load12Phone'))
# This is the 1 org in the Inbound ACD Load testing environment in SCA
# allOrgs.append(('319b38cc-6983-4e89-b82c-a5aea40664b2', 'A00000', 'a1Phone'))
# This is the 1 org in GregS' TCA environment
# allOrgs.append(('a87e93aa-b6fe-4200-bec3-5c4223035db3', 'bac015', 'Phone'))
# This is the 1 org in BrandonD's TCA environment
# allOrgs.append(('373453d1-f220-49c6-9f54-2a8787d3edd8', 'A00000', 'a1Phone'))
# This is the 1 org in the validation environment
allOrgs.append(('e04bb3fc-97f0-4fca-b016-e4d466bd1087', 'acd999', 'Phone'))
# this is the jimo7 org
# allOrgs.append(('96dc9612-e909-4ad0-a398-61aceafe64d9', 'beef00', 'TestPhone'))


domain = 'us-east-1.inintca.com'  # example value is us-east-1.inintca.com
retries = 5  # number of retries per REST request called
timeout = 120  # in Seconds for length of time to wait for any request to get a response


# Classes

#MAC address sequentializer
# to use, define an instance of the class and then do one of two things:
# If you call yourInstance.strictSequentialMac(1, macPrefix) you'll get back each time the next mac to use
# If you call yourInstance.labelBasedMac(youSupplyAValueHere, macPrefix) you'll get back a formatted mac with your supplied value
class macSequence(object):
    def __init__(self):
        self.macCursor = macRangeMin

    def labelBasedMac(self, value, macPrefix):

        useThisMac = macSequence.formatMac(value, macPrefix)

        return useThisMac

    def strictSequentialMac(self, increment, macPrefix):

        useThisMac = macSequence.formatMac(self.macCursor, macPrefix)

        self.macCursor += increment

        if (self.macCursor > macRangeMax):
            print("Max value specified has exceeded! Warning!")

        return useThisMac

    @staticmethod
    def formatMac(cursor, macPrefix):

        # case-style to pad cursor and add prefix
        if 0 <= int(cursor) < 10:
            formattedMac = str(macPrefix) + "00000" + str(cursor)
            return formattedMac
        if 10 <= int(cursor) < 100:
            formattedMac = str(macPrefix) + "0000" + str(cursor)
            return formattedMac
        if 100 <= int(cursor) < 1000:
            formattedMac = str(macPrefix) + "000" + str(cursor)
            return formattedMac
        if 1000 <= int(cursor) < 10000:
            formattedMac = str(macPrefix) + "00" + str(cursor)
            return formattedMac
        if 10000 <= int(cursor) < 100000:
            formattedMac = str(macPrefix) + "0" + str(cursor)
            return formattedMac
        if 100000 <= int(cursor) < 1000000:
            formattedMac = str(macPrefix) + str(cursor)
            return formattedMac

        # if passed value isn't 1-6 digits, inform user
        if int(cursor) >= 1000000 or int(cursor) < 0:
            print("Passed cursor value was: " + str(cursor))
            print("Something went wrong and the length of the desired mac suffix is invalid. Exiting.")
            # some better way to handle this?

# General functions

# function to send GET calls
# code = desired code with default value 200
def getRequest(url, code=200):
    success = 0  # probably not necessary to declare

    # While loop implements total number of retries for timeout and status code
    tries = 0
    while (tries < retries):
        try:
            # actually send request here
            res = requests.get(url, timeout=timeout)

            if (res.status_code == code):
                # success path
                tries = retries
                success = 1
            else:
                # fail for wrong http status code
                tries += 1
                print("")
                print("! Retry needed for GET. Status code: " + str(res.status_code))
                try:
                    print("! Correlation Id: " + str(res.headers['ININ-Correlation-Id']))
                except:
                    print("! -----No correlation Id-----")
                print("")
        except requests.Timeout:
            # fail for timeout
            tries += 1
            print("! Timeout of " + str(timeout) + "s exceeded")
        except:
            # fail for other reasons
            tries += 1
            print("Mysterious issue encountered")
            print(str(res.text))

    return res, success

# function to send DELETE calls
# code = desired code with default value 200
def deleteRequest(url, code=200):
    success = 0 # probably not necessary to declare

    # While loop implements total number of retries for timeout and status code
    tries = 0
    while (tries < retries):
        try:
            # actually send request here
            res = requests.delete(url, timeout=timeout)

            if (res.status_code == code):
                # success path
                tries = retries
                success = 1
            else:
                # fail for wrong http status code
                tries += 1
                print("")
                print("! Retry needed for DELETE. Status code: " + str(res.status_code))
                print("")
        except requests.Timeout:
            # fail for timeout
            tries += 1
            print("Timeout of " + str(timeout) + "s exceeded")
        except:
            # fail for other reasons
            tries += 1
            print("Mysterious issue encountered")
            print(str(res.text))

    return res, success

# function to send PUT calls
# code = desired code with default value 200
def putRequest(url, payload, headers=None, params=None, code=200):
    success = 0 # probably not necessary to declare

    # While loop implements total number of retries for timeout and status code
    tries = 0
    while (tries < retries):
        try:
            # actually send request here
            res = requests.put(url, json=payload, headers=headers, params=params, timeout=timeout)

            if (res.status_code == code):
                # success path
                tries = retries
                success = 1
            else:
                # fail for wrong http status code
                tries += 1
                print("")
                print("! Retry needed for PUT. Status code: " + str(res.status_code))
                print("")
        except requests.Timeout:
            # fail for timeout
            tries += 1
            print("Timeout of " + str(timeout) + "s exceeded")
        except:
            # fail for other reasons
            tries += 1
            print("Mysterious issue encountered")
            print(str(res.text))

    return res, success


# Specific functions

# attempt to get friendly org name because I already have the code written so screw it :)
def getOrgName(orgId):
    getOrgNameConfigUrl = \
            'http://configuration.{0}' \
            '/configurations/v1/organizations/{1}' \
            .format(domain, orgId)

    # call function to send request
    res, success = getRequest(getOrgNameConfigUrl)

    if (success == 1):
        # extract and return orgName
        orgName = res.json()['thirdPartyOrgName']
        return orgName
    else:
        print("! Request to get org name failed in org " + str(orgId) + ". Ignoring failure.")
        return str(orgId)

# get to config private api for phones asc with given page number
def getPhonesConfig(orgId, page):
    getPhonesConfigUrl = \
            'http://configuration.{0}' \
            '/configurations/v2/organizations/{1}/phones?page={2}&sortOrder=asc' \
            .format(domain, orgId, page)

    # call function to make request here
    res, success = getRequest(getPhonesConfigUrl)

    if (success == 1):
        # store totalPages
        totalPages = res.json()['totalPages']

        return res, totalPages
    else:
        print("! Request to get page " + str(page) + " of phones in org " + str(orgId) + "failed. Exiting.")
        # Some better way of handling this?

# Pass in a phone record and receive the numerical label based on the label field
# Also tests that the label matches the provided label
def extractPhoneNumber(record, phoneLabelPrefix):

    # Get label off phone record
    phoneLabel = record["label"]

    # Test for phone label prefix match
    justObservedPrefix = phoneLabel[:int(len(phoneLabelPrefix))]

    if (str(justObservedPrefix) == str(phoneLabelPrefix)):
        match = 1
        # Slice prefix off and pull out phone label number
        justNumber = phoneLabel[int(len(phoneLabelPrefix)):]

    else:
        match = 0
        justNumber = 999999  # Arbitrary value

        print("! Detected prefix non-match")
        print("    phoneLabel: " + str(phoneLabel))
        print("    justObservedPrefix: " + str(justObservedPrefix))
        print("    expected prefix: " + str(phoneLabelPrefix))

    return match, justNumber

# Pass in a phone record and receive the id from the id field
def extractPhoneId(record):
    phoneId = record["id"]
    return phoneId

# update body for phone record
def updateMacInPhoneRecord(record, newMac):
    record["configuration"]["settings"]["phone_hardwareId"] = newMac
    return record

# put phone body
def putPhonesConfig(orgId, phoneId, payload):
    putPhonesConfigUrl = \
            'http://configuration.{0}' \
            '/configurations/v2/organizations/{1}/phones/{2}' \
            .format(domain, orgId, phoneId)

    headers = {'content-type': 'application/json'}

    # call function to make request here
    res, success = putRequest(putPhonesConfigUrl, payload, headers)

    if (success != 1):
        print("! Phone put failed. Response was: ")
        print(str(res))

    return res, success


# For the future

# Maybe provide option for still re-mac-ing phones that don't match pattern using strictSequential?


# Execution

# do this for each org
for i, j, k in allOrgs:  # i is orgId, j is macPrefix, k is phoneLabelPrefix

    macSequencer = macSequence()  # instantiate mac sequencer class for this org

    print("")
    print("")
    print("     ---------- BEGINNING NEW ORG ----------     ")
    print("")
    print("")

    # get number of pages of users in each org
    phonesPage, totalPages = getPhonesConfig(i, '1')
    print("Org " + str(i) + " - Total pages: " + str(totalPages))

    # get orgName
    orgName = getOrgName(i)
    print("Org " + str(i) + " - Name: " + str(orgName))
    print("")

    # get every page of users
    for page in range(totalPages, 0, -1):

        # actually sending get here and getting page of phone records
        phonesPage, totalPages = getPhonesConfig(i, page)
        print("")
        print("Got page " + str(page) + " in Org " + str(orgName))

        # process every record on current page
        for record in phonesPage.json()['content']:

            # print(str(record))
            # print("")

            # only replace mac address if label matches expected prefix
            match, sequenceNumber = extractPhoneNumber(record, k)
            if (match == 1):

                if (phoneRangeMin <= int(sequenceNumber) <= phoneRangeMax):
                    # get new mac address to use
                    newMac = macSequencer.labelBasedMac(sequenceNumber, j)
                    # replace mac address in the record
                    newRecord = updateMacInPhoneRecord(record, newMac)
                    # extract phoneId from the record
                    phoneId = extractPhoneId(record)
                    # put the changed record in cloud (actually sending a put here)
                    res, success = putPhonesConfig(i, phoneId, newRecord)

                    if success == 1:
                        putMacResult = res.json()['configuration']['settings']['phone_hardwareId']
                        print("  SUCCESS " + str(phoneId) + " New Mac: " + str(putMacResult))
                    else:
                        print("")
                        print("! FAILURE on " + str(phoneId))
                        print("")
                        exit()

                    # print(str(res.text))
                    # exit()
                else:
                    print("! Skipping " + k + sequenceNumber + " because number not in range for filter")

            else:
                print("! Skipping record with non-matching label prefix.")

        # Let the user know that the page is processed
        print("  Page processed")

    # Let the user know that the org is processed
    print("Org " + str(orgName) + " PROCESSING COMPLETED")
    print("---------------------------------------------------")

# Space out from next command line
print("")
print("")

