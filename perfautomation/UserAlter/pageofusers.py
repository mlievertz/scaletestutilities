#!/usr/bin/env python

import user
import time

__author__ = 'matt.lievertz'


class PageOfUsers(object):
    """
    Page of Users Object
    """

    def __init__(self, parameters, org_id, org_name, current_page, mysession):
        self.domain = parameters['domain']
        self.phone_pattern = parameters['phone_pattern']
        self.phone_range_correction = parameters['user_range_minimum'] - parameters['phone_range_minimum']
        self.org_id = org_id
        self.org_name = org_name
        self.current_page = current_page
        self.mysession = mysession
        self.users = []

        # Populate page of users
        url = 'https://directory.{0}/directory/v1/organizations/{1}/users?number={2}&sortOrder=asc'.format(
            self.domain,
            self.org_id,
            self.current_page
        )
        response = self.mysession.get(url=url)

        if response:
            for myuser in response.json()['content']:

                # 1) set filter-specific variables
                self.user_pattern = parameters['user_pattern']
                self.user_range_minimum = parameters['user_range_minimum']
                self.user_range_maximum = parameters['user_range_maximum']
                self.user_email_domain = parameters['user_email_domain_override']\
                    if parameters['toggle_user_email_domain_override']\
                    else org_name + '.test'

                # 2) filter logic: match will pass to add user, non-match will continue to skip to next in loop
                user_number = None
                prefix = myuser['email'][0:int(len(self.user_pattern))]
                if prefix == self.user_pattern:
                    no_suffix = myuser['email'][:int(len(myuser['email']) - len('@' + self.user_email_domain))]
                    user_number = no_suffix[int(len(self.user_pattern)):]
                    try:
                        if self.user_range_minimum <= int(user_number) <= self.user_range_maximum:
                            pass
                        else:
                            continue
                    except (TypeError, ValueError) as exception_details:
                        print("! TypeError Exception - substring operations left non-integer")
                        print("  no_suffix: " + no_suffix)
                        print("  user_number: " + user_number)
                        print(str(exception_details))
                else:
                    print("! Automatically skipped - prefix pattern didn't match: " + str(self.user_pattern))
                    print("                                          Instead was: " + str(prefix))
                    print("                                             For user: " + str(myuser['email']))
                    print("")
                    continue  # skip to next user in loop

                # instantiate user and add to list of user objects
                myuser = user.User(
                    user_id=myuser['id'],
                    user_email=myuser['email'],
                    user_number=user_number,
                    org_id=self.org_id,
                    domain=self.domain,
                    mysession=self.mysession
                )
                self.users.append(myuser)

            if not self.users:
                print("! NOTE - no users found on this page")

        else:
            print("! Request to get page of users in org " + str(self.org_id) + "failed.")

    def process(self):
        for myuser in self.users:
            success_code = 0
            attempts = 1
            max_attempts = 5
            delay_factor = 1

            while success_code == 0 & attempts <= max_attempts:
                print("      USER_____" + myuser.user_id + "_EMAIL____" + myuser.user_email)

                default_station_id = myuser.get_matching_station_id(self.phone_pattern, self.phone_range_correction)

                status_code, correlation_id = myuser.put_default_station(default_station_id)

                print("           " + str(status_code) + " " + str(correlation_id) + " DEFAULT STATION PUT")

                time.sleep(delay_factor * ((attempts - 1) ^ 2))

                success_code = myuser.check_default_station(default_station_id)

                if success_code == 0:
                    print("! Verification failed for above operation on attempt " + str(attempts))
                    print("    Will try up to " + str(max_attempts) + " times with waits between attempts")

                attempts += 1

            if attempts > max_attempts:
                print("------------------------------------------------------------------------------------")
                print("! Failed to PUT user default station")
                print("------------------------------------------------------------------------------------")
