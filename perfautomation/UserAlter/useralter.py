#!/usr/bin/env python

import time
import json
import session
import organization
import pageofusers

__author__ = 'matt.lievertz'

'''
TODO:
1. Create logger class and implement/move print() calls to it
'''


def main():
    # Import config.json into parameters
    with open("config.json") as config_file:
        parameters = json.loads(config_file.read())
        parameters['domain'] = parameters['region']+'.inin'+parameters['env']+'.com'
    print("")
    print("BEGINNING PROCESSING")
    print("")

    for org_id in parameters['organizations']:
        # Instantiate the org and the session
        mysession = session.Session('mysession')
        myorganization = organization.Organization(
            parameters=parameters,
            org_id=org_id,
            mysession=mysession
        )

        print("* " + myorganization.org_name + " STARTING")

        # Iterate over pages of users
        for page_number in range(myorganization.get_total_pages_users(), 0, -1):
            user_page = pageofusers.PageOfUsers(
                parameters=parameters,
                org_id=org_id,
                org_name=myorganization.org_name,
                current_page=page_number,
                mysession=mysession
            )
            user_page.process()
            print("    " + str(page_number) + " / " + str(myorganization.total_pages_users) +
                  " USER PAGE PROCESSED in ORG " + myorganization.org_name)

        print("* " + myorganization.org_name + "COMPLETED")
        print("")

    print("COMPLETED PROCESSING")
    print("")


# Execution
if __name__ == '__main__':
    main()
