#!/usr/bin/env python

__author__ = 'matt.lievertz'


class Organization(object):
    """
    Organization Object
    """

    def __init__(self, parameters, org_id, mysession):
        self.domain = parameters['domain']
        self.org_id = org_id
        self.mysession = mysession
        self.total_pages_edges = 0
        self.total_pages_users = 0
        self.total_pages_queues = 0

        try:
            url = 'https://directory.{0}/directory/v1/organizations/{1}'.format(
                self.domain,
                self.org_id
            )
            response = self.mysession.get(
                url=url,
                headers=self.mysession.make_headers()
            )
        except Exception as exception_details:
            response = None
            print("! Failed to get org name in org " + str(self.org_id) + ". Exception details:")
            print(str(exception_details))
            pass
        if response:
            self.org_name = response.json()['thirdPartyOrgName']
        else:
            self.org_name = org_id

    def get_total_pages_users(self):
        url = 'https://directory.{0}/directory/v1/organizations/{1}/users?number={2}&sortOrder=asc'.format(
            self.domain,
            self.org_id,
            '1'
        )
        response = self.mysession.get(
            url=url,
            headers=self.mysession.make_headers()
        )

        if response:
            self.total_pages_users = response.json()['totalPages']
        else:
            print("! Request to get total pages of users in org " + str(self.org_id) + "failed.")

        return self.total_pages_users

    def get_total_pages_edges(self):
        url = 'https://configuration.{0}/configurations/v1/organizations/{1}/edges?page={2}&sortOrder=asc'.format(
            self.domain,
            self.org_id,
            '0'
        )
        response = self.mysession.get(
            url=url,
            headers=self.mysession.make_headers()
        )

        if response:
            self.total_pages_edges = response.json()['totalPages']
        else:
            print("! Request to get total pages of edges in org " + str(self.org_id) + "failed.")

        return self.total_pages_edges


    def get_total_pages_queues(self):
        url = 'https://assignment.{0}/assignment/v2/organizations/{1}/queues?page={2}&sortOrder=asc'.format(
            self.domain,
            self.org_id,
            '1'
        )
        response = self.mysession.get(
            url=url,
            headers=self.mysession.make_headers()
        )

        if response:
            self.total_pages_queues = response.json()['totalPages']
        else:
            print("! Request to get total pages of queues in org " + str(self.org_id) + "failed.")

        return self.total_pages_queues