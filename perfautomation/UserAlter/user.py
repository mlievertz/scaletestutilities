#!/usr/bin/env python

__author__ = 'matt.lievertz'


class User(object):
    """
    User object
    """

    def __init__(self, user_id, user_email, user_number, org_id, domain, mysession):
        self.user_id = user_id
        self.user_email = user_email
        self.user_number = user_number
        self.org_id = org_id
        self.domain = domain
        self.mysession = mysession
        self.user_conversations = []

    def get_conversations(self):
        url = 'https://conversation.{0}/conversations/organizations/{1}/users/{2}/conversations'.format(
            self.domain,
            self.org_id,
            self.user_id
        )
        response = self.mysession.get(
            url=url,
            headers=self.mysession.make_headers()
        )

        if len(response.text) >= 36:
            for conversation in response.json():

                self.user_conversations.append(conversation['id'])

            return self.user_conversations

        else:
            return None

    def delete_conversations(self):
        url = 'https://conversation.{0}/conversations/organizations/{1}/users/{2}/conversations'.format(
            self.domain,
            self.org_id,
            self.user_id
        )
        response = self.mysession.delete(
            url=url,
            headers=self.mysession.make_headers()
        )

        # conversation DELETE endpoint does not give a correlation ID, still keeping stub in
        correlation_id = "        -    -    -    -            "

        return response.status_code, correlation_id

    def delete_assignment(self):
        url = 'https://assignment.{0}/assignment/organizations/{1}/agents/{2}'.format(
            self.domain,
            self.org_id,
            self.user_id
        )
        response = self.mysession.delete(
            url=url,
            headers=self.mysession.make_headers()
        )

        try:
            correlation_id = response.headers['ININ-Correlation-Id']
        except (KeyError, AttributeError) as exception_details:
            correlation_id = "NO CORRELATION ID"
            print("! Delete assignment gave no correlation ID")
            print(str(exception_details))
            pass

        return response.status_code, correlation_id

    def delete_phone_association(self):
        url = 'https://user.{0}/user/v2/organizations/{1}/users/{2}/associatedStation'.format(
            self.domain,
            self.org_id,
            self.user_id
        )
        response = self.mysession.delete(
            url=url,
            headers=self.mysession.make_headers()
        )

        try:
            correlation_id = response.headers['ININ-Correlation-Id']
        except (KeyError, AttributeError) as exception_details:
            correlation_id = "NO CORRELATION ID"
            print("! Delete station association gave no correlation ID")
            print(str(exception_details))
            pass

        return response.status_code, correlation_id

    def get_matching_station_id(self, phone_pattern, phone_range_correction):
        station_suffix = '_0'
        url = 'https://configuration.{0}/configurations/v2/organizations/{1}/stations' \
            '?sortField=label&sortOrder=desc&filters[label]={2}{3}{4}'.format(
                self.domain,
                self.org_id,
                phone_pattern,
                str(int(self.user_number) - int(phone_range_correction)),
                station_suffix
            )
        response = self.mysession.get(
            url=url,
            headers=self.mysession.make_headers()
        )

        if response:
            try:
                matching_station_id = response.json()['content'][0]['id']
            except (KeyError, AttributeError) as exception_details:
                print("! Didn't get back a station id where expected")
                print("")
                print(response.text)
                print("")
                print(str(exception_details))
                pass
        else:
            # Need a plan to handle failure - this could eventually be a list of reserve stations or just building and
            # exporting a list of users that the user needs to fix manually
            print("! NOTE - no matching stations found")

        return matching_station_id

    def put_default_station(self, default_station_id):
        url = 'https://user.{0}/user/v1/organizations/{1}/users/{2}/defaultStation/{3}'.format(
            self.domain,
            self.org_id,
            self.user_id,
            default_station_id
        )
        response = self.mysession.put(
            url=url,
            payload=None,
            headers=self.mysession.make_headers()
        )

        try:
            correlation_id = response.headers['ININ-Correlation-Id']
        except (KeyError, AttributeError) as exception_details:
            correlation_id = "NO CORRELATION ID"
            print("! Put default station gave no correlation ID")
            print(str(exception_details))
            pass

        return response.status_code, correlation_id

    def check_default_station(self, correct_station_id):
        url = 'https://user.{0}/user/v1/organizations/{1}/users/{2}'.format(
            self.domain,
            self.org_id,
            self.user_id
        )
        response = self.mysession.get(
            url=url,
            headers=self.mysession.make_headers()
        )

        success_value = 0

        if response:
            try:
                actual_station_id = response.json()['defaultStationId']
                if actual_station_id == correct_station_id:
                    success_value = 1
            except (KeyError, AttributeError) as exception_details:
                print("! User Service record did not include a defaultStationId field")
                print(str(exception_details))
                pass

        else:
            print("! Could not verify default station - no response to GET to User Service")

        return success_value
