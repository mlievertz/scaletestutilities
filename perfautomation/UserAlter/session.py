#!/usr/bin/env python

import requests
import socket
import time

__author__ = 'matt.lievertz'
# borrows some from Evan.Ellis


class Session(object):
    """Creates a session object to perform REST calls"""
    def __init__(self, session_name):
        self.session_name = session_name
        self.retries = 5
        self.timeout = 120  # seconds
        self.delay_factor = 1  # seconds - base value for exponential back-off for 429s

    # This object can hold your cookies!
    s = requests.Session()

    # function to make headers
    def make_headers(self, additional_header=None):
        # get localhost IP for user-agent
        try:
            socket_instance = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            socket_instance.connect(("8.8.8.8", 80))
            localhost_ip = socket_instance.getsockname()[0]
            socket_instance.close()
        except (socket.error, Exception) as exception_details:
            localhost_ip = "localhost"
            print("! Got an exception trying to get localhost IP for user-agent. Will continue using \"localhost\".")
            print(str(exception_details))

        # make default header
        headers = {
            'content-type': 'application/json',
            'accept': 'application/json',
            'user-agent': 'Python-Requests-MattLievertz/2 (' + str(localhost_ip) + ')'
        }

        # attach custom header if supplied
        if additional_header:
            headers.update(additional_header)

        return headers

    # function to send GET calls
    def get(self, url, headers=None, params=None):
        tries = 0
        res = None

        while tries < self.retries:
            try:
                res = self.s.get(
                    url=url,
                    headers=headers,
                    params=params,
                    timeout=self.timeout
                )

                if res.status_code == 200:
                    tries = self.retries
                elif res.status_code == 429 or res.status_code == 400:
                    time.sleep(self.delay_factor * (tries^2))
                    tries +=1
                else:
                    tries += 1
                    print("")
                    print("! Retry needed for GET. URL:")
                    print("! " + str(url))
                    print("! Status code: " + str(res.status_code))

                    try:
                        print("! Correlation Id: " + str(res.headers['ININ-Correlation-Id']))
                    except (KeyError, AttributeError) as exception_details:
                        print("! -----No correlation Id-----")
                        print(str(exception_details))
                        pass
                    print("")
            except requests.exceptions.Timeout:
                tries += 1
                print("! Timeout of " + str(self.timeout) + "s exceeded")
                pass
            except requests.exceptions.ConnectionError as exception_details:
                print("! A connection error occurred, likely due to a proxy or incorrect http/https in url")
                print(str(exception_details))
                pass
            except Exception as exception_details:
                tries += 1
                print("! Mysterious issue encountered. Details:")
                print(str(exception_details))
                pass

        return res

    # function to send POST calls
    def post(self, url, payload, headers=None, params=None):
        tries = 0
        res = None

        while tries < self.retries:
            try:
                res = self.s.post(
                    url=url,
                    json=payload,
                    headers=headers,
                    params=params,
                    timeout=self.timeout
                )

                if res.status_code == 200:
                    tries = self.retries
                elif res.status_code == 429 or res.status_code == 400:
                    time.sleep(self.delay_factor * (tries^2))
                    tries +=1
                else:
                    tries += 1
                    print("")
                    print("! Retry needed for POST. URL:")
                    print("! " + str(url))
                    print("! Status code: " + str(res.status_code))
                    try:
                        print("! Correlation Id: " + str(res.headers['ININ-Correlation-Id']))
                    except (KeyError, AttributeError) as exception_details:
                        print("! -----No correlation Id-----")
                        print(str(exception_details))
                        pass
                    print("")
            except requests.exceptions.Timeout:
                tries += 1
                print("! Timeout of " + str(self.timeout) + "s exceeded")
                pass
            except requests.exceptions.ConnectionError as exception_details:
                print("! A connection error occurred, likely due to a proxy or incorrect http/https in url")
                print(str(exception_details))
                pass
            except Exception as exception_details:
                tries += 1
                print("! Mysterious issue encountered. Details:")
                print(str(exception_details))
                pass

        return res

    # function to send PUT calls
    def put(self, url, payload, headers=None, params=None):
        tries = 0
        res = None

        while tries < self.retries:
            try:
                res = self.s.put(
                    url=url,
                    json=payload,
                    headers=headers,
                    params=params,
                    timeout=self.timeout
                )

                if res.status_code == 200:
                    tries = self.retries
                elif res.status_code == 429 or res.status_code == 400:
                    time.sleep(self.delay_factor * (tries^2))
                    tries +=1
                else:
                    tries += 1
                    print("")
                    print("! Retry needed for PUT. URL:")
                    print("! " + str(url))
                    print("! Status code: " + str(res.status_code))
                    try:
                        print("! Correlation Id: " + str(res.headers['ININ-Correlation-Id']))
                    except (KeyError, AttributeError) as exception_details:
                        print("! -----No correlation Id-----")
                        print(str(exception_details))
                        pass
                    print("")
            except requests.exceptions.Timeout:
                tries += 1
                print("! Timeout of " + str(self.timeout) + "s exceeded")
                pass
            except requests.exceptions.ConnectionError as exception_details:
                print("! A connection error occurred, likely due to a proxy or incorrect http/https in url")
                print(str(exception_details))
                pass
            except Exception as exception_details:
                tries += 1
                print("! Mysterious issue encountered. Details:")
                print(str(exception_details))
                pass

        return res

    # function to send DELETE calls
    def delete(self, url, headers=None, params=None):
        tries = 0
        res = None

        while tries < self.retries:
            try:
                res = self.s.delete(
                    url=url,
                    headers=headers,
                    params=params,
                    timeout=self.timeout
                )

                if res.status_code == 200:
                    tries = self.retries
                elif res.status_code == 429 or res.status_code == 400:
                    time.sleep(self.delay_factor * (tries^2))
                    tries +=1
                else:
                    tries += 1
                    print("")
                    print("! Retry needed for DELETE. URL:")
                    print("! " + str(url))
                    print("! Status code: " + str(res.status_code))
                    try:
                        print("! Correlation Id: " + str(res.headers['ININ-Correlation-Id']))
                    except (KeyError, AttributeError) as exception_details:
                        print("! -----No correlation Id-----")
                        print(str(exception_details))
                        pass
                    print("")
            except requests.exceptions.Timeout:
                tries += 1
                print("! Timeout of " + str(self.timeout) + "s exceeded")
                pass
            except requests.exceptions.ConnectionError as exception_details:
                print("! A connection error occurred, likely due to a proxy or incorrect http/https in url")
                print(str(exception_details))
                pass
            except Exception as exception_details:
                tries += 1
                print("! Mysterious issue encountered. Details:")
                print(str(exception_details))
                pass

        return res
