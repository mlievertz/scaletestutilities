This script will take an org that follows fairly standard testing naming conventions...

(specifically:
   1. User email domain is {org name}.test - this can be overridden in config
   2. Users emails follow numerical sequential order - e.g., User0@domain.test, User1@domain.test
   3. Station labels follow numerical sequential order and equal {phonename}_0 - e.g., Phone0_0, Phone1_0, Phone2_0)

And it will give each UserX a default station that has the matching numerical label PhoneY_0. The X:Y relationship is
defined by the relationship between the minimum user range and minimum phone range (i.e., the min value of user gets
the min value of phone; and sequential up from there -- if both ranges start on the same number X=Y).

This is for use with new BIS functionality that will check and associate a user's default phone rather than using the
old round-robin list.

Requires:

+ Python3 (download at https://www.python.org/downloads/)
+ Requests (at path, type 'pip3 install requests')
+ Download all the files in this git folder onto your localhost to run it from
+ Must be logged into VPC through VPN  at runtime
+ Make config changes in config.json file following numbered instructions below
+ When ready, execute from folder you downloaded files into with: python3 useralter.py

Instructions:

1. Change contents of "organizations":[] to include your org id(s) like:
    [
        "id",
        "id2",
        "id3"
    ]

2. This script assumes the user is using standard naming conventions. Fill in the base pattern for your user emails
   and phone names - e.g., "User" for User0, User1, ..., UserN or "Phone" for Phone0, Phone1, ..., PhoneN.

3. If you want to limit the number of users out of your org that you operate on, specify a numerical range.
   E.g. Specify Min: 100 Max: 499 and the script will operate on User100 to User499 only

4. The rest of the config fields have niche uses:
   a. Env/region - use if operating in different environment or region
   b. toggle_user_email_domain_override can be set if your user email domain is not {your Org Name}.test
      This shouldn't be the case, but if it is, you can provide the email domain in the next field

5. If you want to store some values or notes in the config file so you don't have to look somewhere else next time you
   switch between frequently-used orgs or some such thing, you can do so in the provided field - just make sure you use
   valid JSON.


Helpful Notes:

1. The script filters are case-sensitive.