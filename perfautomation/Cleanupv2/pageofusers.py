#!/usr/bin/env python

import user

__author__ = 'matt.lievertz'


class PageOfUsers(object):
    """
    Page of Users Object
    """

    def __init__(self, parameters, org_id, org_name, current_page, mysession):
        self.domain = parameters['domain']
        self.toggle_check_conversations = parameters['toggle_check_conversations']
        self.toggle_delete_conversations = parameters['toggle_delete_conversations']
        self.toggle_delete_station_associations = parameters['toggle_delete_station_associations']
        self.org_id = org_id
        self.org_name = org_name
        self.current_page = current_page
        self.mysession = mysession
        self.users = []

        # Populate page of users
        url = 'https://directory.{0}/directory/v1/organizations/{1}/users?number={2}&sortOrder=asc'.format(
            self.domain,
            self.org_id,
            self.current_page
        )
        response = self.mysession.get(url=url)

        if response:
            for myuser in response.json()['content']:

                if parameters['toggle_filter_users']:
                    # 1) set filter-specific variables
                    self.user_pattern = parameters['user_pattern']
                    self.user_range_minimum = parameters['user_range_minimum']
                    self.user_range_maximum = parameters['user_range_maximum']
                    self.user_email_domain = parameters['user_email_domain_override']\
                        if parameters['toggle_user_email_domain_override']\
                        else org_name + '.test'

                    # 2) filter logic: match will pass to add user, non-match will continue to skip to next in loop
                    prefix = myuser['email'][0:int(len(self.user_pattern))]
                    if prefix == self.user_pattern:
                        no_suffix = myuser['email'][:int(len(myuser['email']) - len('@' + self.user_email_domain))]
                        user_number = no_suffix[int(len(self.user_pattern)):]
                        try:
                            if self.user_range_minimum <= int(user_number) <= self.user_range_maximum:
                                pass
                            else:
                                continue
                        except (TypeError, ValueError) as exception_details:
                            print("! TypeError Exception - substring operations left non-integer")
                            print("  no_suffix: " + no_suffix)
                            print("  user_number: " + user_number)
                            print(str(exception_details))
                    else:
                        print("! Automatically skipped - prefix pattern didn't match: " + str(self.user_pattern))
                        print("                                          Instead was: " + str(prefix))
                        print("                                             For user: " + str(myuser['email']))
                        print("")
                        continue  # skip to next user in loop

                # instantiate user and add to list of user objects
                myuser = user.User(
                    user_id=myuser['id'],
                    user_email=myuser['email'],
                    org_id=self.org_id,
                    domain=self.domain,
                    mysession=self.mysession
                )
                self.users.append(myuser)

            if not self.users:
                print("! NOTE - no users found on this page")

        else:
            print("! Request to get page of users in org " + str(self.org_id) + "failed.")

    def process(self):
        for myuser in self.users:
            user_conversations = None

            print("      USER_____" + myuser.user_id + "_EMAIL____" + myuser.user_email)

            # 1) Send a check for conversations if toggles say to
            if self.toggle_check_conversations:

                user_conversations = myuser.get_conversations()

                if user_conversations:
                    for conversation in user_conversations:
                        print("")
                        print("!   Detected conversation " + conversation + " on " + myuser.user_email)
                        print("")
                else:
                    print("                       -    -    -    -                    - No Conversations Found")

            # 2) Send conversation deletes if appropriate toggles say to and if relevant if there is a conversation
            if (self.toggle_delete_conversations and not self.toggle_check_conversations) or\
                    (self.toggle_delete_conversations and user_conversations):

                status_code, correlation_id = myuser.delete_conversations()
                print("           " + str(status_code) + " " + str(correlation_id) + " DELETE - Conversation")

                status_code, correlation_id = myuser.delete_assignment()
                print("           " + str(status_code) + " " + str(correlation_id) + " DELETE - Assignment")

            # 3) Send station association deletes if toggle says to
            if self.toggle_delete_station_associations:

                status_code, correlation_id = myuser.delete_phone_association()
                print("           " + str(status_code) + " " + str(correlation_id) + " DELETE - Phone Association")
