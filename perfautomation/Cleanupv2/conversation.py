#!/usr/bin/env python

__author__ = 'matt.lievertz'


class Conversation(object):
    """
    Conversation object
    """

    def __init__(self, conversation_id, org_id, domain, mysession):
        self.conversation_id = conversation_id
        self.org_id = org_id
        self.domain = domain
        self.mysession = mysession

    def delete_in_conversation(self):
        url = 'https://conversation.{0}/conversations/{1}'.format(
            self.domain,
            self.conversation_id
        )
        response = self.mysession.delete(
            url=url,
            headers=self.mysession.make_headers()
        )

        # conversation DELETE endpoint does not give a correlation ID, still keeping stub in
        correlation_id = "        -    -    -    -            "

        return response.status_code, correlation_id

    def delete_in_assignment(self):
        url = 'https://assignment.{0}/assignment/organizations/{1}/requests/{2}'.format(
            self.domain,
            self.org_id,
            self.conversation_id
        )
        response = self.mysession.delete(
            url=url,
            headers=self.mysession.make_headers()
        )

        try:
            correlation_id = response.headers['ININ-Correlation-Id']
        except (KeyError, AttributeError) as exception_details:
            correlation_id = "NO CORRELATION ID"
            print("! Delete conversation in assignment gave no correlation ID")
            print(str(exception_details))
            pass

        return response.status_code, correlation_id