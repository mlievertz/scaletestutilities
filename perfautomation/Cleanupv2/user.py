#!/usr/bin/env python

__author__ = 'matt.lievertz'


class User(object):
    """
    User object
    """

    def __init__(self, user_id, user_email, org_id, domain, mysession):
        self.user_id = user_id
        self.user_email = user_email
        self.org_id = org_id
        self.domain = domain
        self.mysession = mysession
        self.user_conversations = []

    def get_conversations(self):
        url = 'https://conversation.{0}/conversations/organizations/{1}/users/{2}/conversations'.format(
            self.domain,
            self.org_id,
            self.user_id
        )
        response = self.mysession.get(
            url=url,
            headers=self.mysession.make_headers()
        )

        if len(response.text) >= 36:
            for conversation in response.json():

                self.user_conversations.append(conversation['id'])

            return self.user_conversations

        else:
            return None

    def delete_conversations(self):
        url = 'https://conversation.{0}/conversations/organizations/{1}/users/{2}/conversations'.format(
            self.domain,
            self.org_id,
            self.user_id
        )
        response = self.mysession.delete(
            url=url,
            headers=self.mysession.make_headers()
        )

        # conversation DELETE endpoint does not give a correlation ID, still keeping stub in
        correlation_id = "        -    -    -    -            "

        return response.status_code, correlation_id

    def delete_assignment(self):
        url = 'https://assignment.{0}/assignment/organizations/{1}/agents/{2}'.format(
            self.domain,
            self.org_id,
            self.user_id
        )
        response = self.mysession.delete(
            url=url,
            headers=self.mysession.make_headers()
        )

        try:
            correlation_id = response.headers['ININ-Correlation-Id']
        except (KeyError, AttributeError) as exception_details:
            correlation_id = "NO CORRELATION ID"
            print("! Delete assignment gave no correlation ID")
            print(str(exception_details))
            pass

        return response.status_code, correlation_id

    def delete_phone_association(self):
        url = 'https://user.{0}/user/v2/organizations/{1}/users/{2}/associatedStation'.format(
            self.domain,
            self.org_id,
            self.user_id
        )
        response = self.mysession.delete(
            url=url,
            headers=self.mysession.make_headers()
        )

        try:
            correlation_id = response.headers['ININ-Correlation-Id']
        except (KeyError, AttributeError) as exception_details:
            correlation_id = "NO CORRELATION ID"
            print("! Delete station association gave no correlation ID")
            print(str(exception_details))
            pass

        return response.status_code, correlation_id
