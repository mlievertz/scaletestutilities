Requires:

+ Python3 (download at https://www.python.org/downloads/)
+ Requests (at path, type 'pip3 install requests')
+ Download all the files in this git folder onto your localhost to run it from
+ Must be logged into VPC through VPN  at runtime
+ Make config changes in config.json file following numbered instructions below
+ When ready, execute from folder you downloaded files into with: python3 cleanupv2.py

1. Change contents of "organizations":[] to include your org id(s) like:
    [
        "id",
        "id2",
        "id3"
    ]

    *All further config options are optional*

2. You can choose what parts of the script to use - by default it will delete conversations on users and queues and also
   delete station associations on users. To also restart edges change "toggle_operate_on_edges" to true. Changing
   "toggle_operate_on_users" or "toggle_operate_on_queues" to false will cause no operations to run on the indicated
   type of resource.

3. If your org is at all large, the script will run a lot faster if you filter on just the users and queues you are
   actively using in your tests. To enable filtering, change "toggle_filter_users" and/or "toggle_filter_queues" to
   true and then fill in the rest of the appropriate config options for filtering (pattern, range_minimum, and
   range_maximum).

4. You can toggle on and off whether the script deletes conversation and/or deletes station associations with the
   toggles: "toggle_delete_conversations" and "toggle_delete_station_associations"

5. The script has a behavior to first check if a user has conversations before deleting. If you want, you can disable
   this to cause the script to send deletes on all users without checking first. This would be faster if you expect
   most users to have conversations or are just paranoid.

6. If your org name doesn't match your user email domains then you can supply that variable and flip the toggle to use
   it.  This wouldn't be best practice, but whatever. Otherwise the script just grabs your org name so you don't need
   to mess with it.
