#!/usr/bin/env python

import edge

__author__ = 'matt.lievertz'


class PageOfEdges(object):
    """
    Page of Edges Object
    """

    def __init__(self, parameters, org_id, current_page, mysession):
        self.domain = parameters['domain']
        self.org_id = org_id
        self.current_page = current_page
        self.mysession = mysession
        self.edges = []

        # Populate page of edges
        url = 'https://configuration.{0}/configurations/v1/organizations/{1}/edges?page={2}&sortOrder=asc'.format(
            self.domain,
            self.org_id,
            self.current_page
        )
        response = self.mysession.get(
            url=url,
            headers=self.mysession.make_headers()
        )

        if response:
            for myedge in response.json()['content']:
                # instantiate edge and add to list of edge objects
                edge_object = edge.Edge(
                    edge_id=myedge['id'],
                    edge_name=myedge['label'],
                    org_id=self.org_id,
                    domain=self.domain,
                    mysession=self.mysession
                )
                self.edges.append(edge_object)

            if not self.edges:
                print("! NOTE - no edges found on this page")

        else:
            print("! Request to get page of edges in org " + str(self.org_id) + "failed.")

    def process(self):
        for myedge in self.edges:
            print("      EDGE_____" + myedge.edge_id + "_NAME____" + myedge.edge_name)
            status_code, correlation_id = myedge.restart_edge()
            print("           " + str(status_code) + " " + str(correlation_id) + " RESTART")