#!/usr/bin/env python

import conversation
import time

__author__ = 'matt.lievertz'


class PageOfConversations(object):
    """
    Page of Edges Object
    """

    def __init__(self, toggle_delete_conversations, queue_id, queue_name, org_id, domain, mysession, current_page):
        self.toggle_delete_conversations = toggle_delete_conversations
        self.queue_id = queue_id
        self.queue_name = queue_name
        self.org_id = org_id
        self.domain = domain
        self.mysession = mysession
        self.current_page = current_page
        self.conversations = []

        # populate page of conversations
        url = 'https://assignment.{0}/assignment/v2/organizations/{1}/queues/{2}/conversations?' \
              'page={3}&sortOrder=asc'.format(
                self.domain,
                self.org_id,
                self.queue_id,
                self.current_page
              )
        response = self.mysession.get(
            url=url,
            headers=self.mysession.make_headers()
        )

        if response:
            for myconversation in response.json()['content']:
                # instantiate conversation and add to list of conversation objects
                conversation_object = conversation.Conversation(
                    conversation_id=myconversation['conversationId'],
                    org_id=self.org_id,
                    domain=self.domain,
                    mysession=self.mysession
                )
                self.conversations.append(conversation_object)

            if not self.conversations:
                print("! NOTE - no conversations found on this page")

        else:
            print("! Request to get page of conversations in org " + str(self.org_id) + "failed.")

    def process(self):
        for myconversation in self.conversations:
            print("!       FOUND CONVERSATION " + myconversation.conversation_id + " on queue " + self.queue_name)

            if self.toggle_delete_conversations:
                status_code, correlation_id = myconversation.delete_in_conversation()
                print("           " + str(status_code) + " " + str(correlation_id) + " DELETE - in Conversation")

    def process_again(self):
        for myconversation in self.conversations:
            print("!       FOUND CONVERSATION " + myconversation.conversation_id + " on queue " + self.queue_name)

            if self.toggle_delete_conversations:
                status_code, correlation_id = myconversation.delete_in_conversation()
                print("           " + str(status_code) + " " + str(correlation_id) + " DELETE - in Conversation")

                time.sleep(1)  # this is too long a delay to use often, but typically this function won't be called

                status_code, correlation_id = myconversation.delete_in_assignment()
                print("           " + str(status_code) + " " + str(correlation_id) + " DELETE - in Assignment")
