#!/usr/bin/env python

import acdqueue

__author__ = 'matt.lievertz'


class PageOfQueues(object):
    """
    Page of Queues Object
    """

    def __init__(self, parameters, org_id, current_page, mysession):
        self.domain = parameters['domain']
        self.toggle_delete_conversations = parameters['toggle_delete_conversations']
        self.org_id = org_id
        self.current_page = current_page
        self.mysession = mysession
        self.acdqueues = []

        # Populate page of queues
        url = 'https://assignment.{0}/assignment/v2/organizations/{1}/queues?page={2}&sortOrder=asc'.format(
            self.domain,
            self.org_id,
            self.current_page
        )
        response = self.mysession.get(
            url=url,
            headers=self.mysession.make_headers()
        )

        if response:
            for myqueue in response.json()['content']:

                if parameters['toggle_filter_queues']:
                    # 1) set filter-specific variables
                    self.queue_pattern = parameters['queue_pattern']
                    self.queue_range_minimum = parameters['queue_range_minimum']
                    self.queue_range_maximum = parameters['queue_range_maximum']

                    # 2) filter logic: match will pass to add queue, non-match will continue to skip to next in loop
                    prefix = myqueue['name'][0:int(len(self.queue_pattern))]
                    if prefix == self.queue_pattern:
                        queue_number = myqueue['name'][int(len(self.queue_pattern)):]
                        try:
                            if self.queue_range_minimum <= int(queue_number) <= self.queue_range_maximum:
                                pass
                            else:
                                continue
                        except (TypeError, ValueError) as exception_details:
                            print("! Substring operations left non-integer")
                            print("  queue_number variable value: " + queue_number)
                            print(str(exception_details))
                    else:
                        print("      QUEUE " + str(myqueue['name']) + " parsed as " + str(prefix) +
                              " - NO MATCH to pattern: " + str(self.queue_pattern))
                        continue  # skip to next queue in loop

                # instantiate queue and add to list of queue objects
                myqueue_object = acdqueue.ACDQueue(
                    toggle_delete_conversations=self.toggle_delete_conversations,
                    queue_id=myqueue['id'],
                    queue_name=myqueue['name'],
                    org_id=self.org_id,
                    domain=self.domain,
                    mysession=self.mysession
                )
                self.acdqueues.append(myqueue_object)

            if not self.acdqueues:
                print("! NOTE - no users found on this page")

        else:
            print("! Request to get page of queues in org " + str(self.org_id) + "failed.")

    def process(self):
        for myqueue in self.acdqueues:

            print("      QUEUE____" + myqueue.queue_id + "_NAME_____" + myqueue.queue_name)
            myqueue.process()
