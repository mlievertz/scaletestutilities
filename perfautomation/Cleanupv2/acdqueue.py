#!/usr/bin/env python

import pageofconversations
import time

__author__ = 'matt.lievertz'


class ACDQueue(object):
    """
    Queue Object
    """

    def __init__(self, toggle_delete_conversations, queue_id, queue_name, org_id, domain, mysession):
        self.toggle_delete_conversations = toggle_delete_conversations
        self.queue_id = queue_id
        self.queue_name = queue_name
        self.org_id = org_id
        self.domain = domain
        self.mysession = mysession
        self.total_pages_conversations = 0

    def get_total_pages_of_conversations(self):
        url = 'https://assignment.{0}/assignment/v2/organizations/{1}/queues/{2}/conversations?' \
              'page={3}&sortOrder=asc'.format(
            self.domain,
            self.org_id,
            self.queue_id,
            '1'
        )
        response = self.mysession.get(
            url=url,
            headers=self.mysession.make_headers()
        )

        if response:
            self.total_pages_conversations = response.json()['totalPages']
        else:
            print("! Request to get total pages of conversations in queue " + str(self.queue_name) + "failed.")

    def process(self):
        self.get_total_pages_of_conversations()

        if self.total_pages_conversations > 0:

            # 1) Ideally we want to delete conversation in conversation, which should also take it out of assignment
            for page_number in range(self.total_pages_conversations, 0, -1):
                conversation_page = pageofconversations.PageOfConversations(
                    toggle_delete_conversations=self.toggle_delete_conversations,
                    queue_id=self.queue_id,
                    queue_name=self.queue_name,
                    org_id=self.org_id,
                    domain=self.domain,
                    mysession=self.mysession,
                    current_page=page_number
                )
                conversation_page.process()
                print("        " + str(page_number) + " / " + str(self.total_pages_conversations) +
                      " CONVERSATION PAGE PROCESSED in QUEUE " + self.queue_name)

            # 2) Double-check that conversations are really gone (only need to do if deletes are toggled on)
            if self.toggle_delete_conversations:
                print("        --DOUBLE-CHECKING All CONVERSATIONS GONE...")
                time.sleep(5)

                self.get_total_pages_of_conversations()

                # 3) If conversations are somehow persistent, try again and also delete directly in assignment this time
                for page_number in range(self.total_pages_conversations, 0, -1):
                    conversation_page = pageofconversations.PageOfConversations(
                        toggle_delete_conversations=self.toggle_delete_conversations,
                        queue_id=self.queue_id,
                        queue_name=self.queue_name,
                        org_id=self.org_id,
                        domain=self.domain,
                        mysession=self.mysession,
                        current_page=page_number
                    )
                    conversation_page.process_again()
                    print("        " + str(page_number) + " / " + str(self.total_pages_conversations) +
                          " CONVERSATION PAGE PROCESSED in QUEUE " + self.queue_name)
        else:
            print("        NO CONVERSATIONS FOUND in QUEUE " + self.queue_name)