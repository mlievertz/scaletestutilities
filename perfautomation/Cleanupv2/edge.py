#!/usr/bin/env python

__author__ = 'matt.lievertz'


class Edge(object):
    """
    Edge object
    """

    def __init__(self, edge_id, edge_name, org_id, domain, mysession):
        self.edge_id = edge_id
        self.edge_name = edge_name
        self.org_id = org_id
        self.domain = domain
        self.mysession = mysession

    def restart_edge(self):
        url = 'https://update.{0}/update/v1/edges/{1}/reboot'.format(
            self.domain,
            self.edge_id
        )
        org_header = {'ININ-Organization-Id': self.org_id}
        response = self.mysession.post(
            url=url,
            payload="",
            headers=org_header
        )

        try:
            correlation_id = response.headers['ININ-Correlation-Id']
        except (KeyError, AttributeError) as exception_details:
            correlation_id = "NO CORRELATION ID"
            print("! No correlation ID when trying to restart edge")
            print(str(exception_details))
            pass
        return response.status_code, correlation_id
