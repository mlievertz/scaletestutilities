#!/usr/bin/env python

import time
import json
import session
import organization
import pageofedges
import pageofusers
import pageofqueues

__author__ = 'matt.lievertz'

'''
TODO:
1. Create logger class and implement/move print() calls to it
2. Stats and feedback about deletes, etc.?, possibly as a stat-keeper class
'''


def main():
    # 0) Import config.json into parameters
    with open("config.json") as config_file:
        parameters = json.loads(config_file.read())
        parameters['domain'] = parameters['region']+'.inin'+parameters['env']+'.com'
    print("")
    print("BEGINNING CLEANUP")
    print("")

    for org_id in parameters['organizations']:
        # 1) Instantiate the org and the session
        mysession = session.Session('mysession')
        myorganization = organization.Organization(
            parameters=parameters,
            org_id=org_id,
            mysession=mysession
        )

        print("* " + myorganization.org_name + " STARTING")

        # 2) Process Queues - must do prior to edge restarts b/c calls will rely on edge reply
        if parameters['toggle_operate_on_queues']:
            print("  * " + myorganization.org_name + " QUEUE PROCESSING BEGIN")
            for page_number in range(myorganization.get_total_pages_queues(), 0, -1):
                queue_page = pageofqueues.PageOfQueues(
                    parameters=parameters,
                    org_id=org_id,
                    current_page=page_number,
                    mysession=mysession
                )
                queue_page.process()
                print("    " + str(page_number) + " / " + str(myorganization.total_pages_queues) +
                      " QUEUE PAGE PROCESSED in ORG " + myorganization.org_name)

        # 3) Process Edges
        if parameters['toggle_operate_on_edges']:
            print("  * " + myorganization.org_name + " EDGE PROCESSING BEGIN")
            time.sleep(5)  # give all script-generated edge-reliant calls a chance to clear system before continuing
            for page_number in range(myorganization.get_total_pages_edges(), 0, -1):
                edge_page = pageofedges.PageOfEdges(
                    parameters=parameters,
                    org_id=org_id,
                    current_page=page_number,
                    mysession=mysession
                )
                edge_page.process()
                print("    " + str(page_number) + " / " + str(myorganization.total_pages_edges) +
                      " EDGE PAGE PROCESSED in ORG " + myorganization.org_name)

        # 4) Process Users
        if parameters['toggle_operate_on_users']:
            print("  * " + myorganization.org_name + " USER PROCESSING BEGIN")
            for page_number in range(myorganization.get_total_pages_users(), 0, -1):
                user_page = pageofusers.PageOfUsers(
                    parameters=parameters,
                    org_id=org_id,
                    org_name=myorganization.org_name,
                    current_page=page_number,
                    mysession=mysession
                )
                user_page.process()
                print("    " + str(page_number) + " / " + str(myorganization.total_pages_users) +
                      " USER PAGE PROCESSED in ORG " + myorganization.org_name)

        print("* " + myorganization.org_name + "COMPLETED")
        print("")

    print("COMPLETED CLEANUP")
    print("")


# Execution
if __name__ == '__main__':
    main()
