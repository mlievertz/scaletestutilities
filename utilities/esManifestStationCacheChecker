#!/bin/bash

#NOTE ON DEPENDENCIES: relies on jsawk and having a bastion host connection to the VPC and ssh config so that http://localhost:8205 redirects to TCA config service.
#Get jsawk here: https://github.com/micha/jsawk#setup
#Follow the install/setup instructions as noted.
#Essentially jsawk needs Spidermonkey, which can be gotten using brew or macports, and some of the previous need xcode on mac, I think.
#jsawk is a bash script that you download, make executable, and either place in your path or in /bin or link from there.
#curl should come standard, so you probably already have it.

#some variables
orgId=231e01e8-c97c-4335-87f1-753b0c3d2c24
edgeOne=2d2c8d2b-b675-46f3-a421-9520b5f0a741
edgeTwo=25542516-16c0-415c-be79-62a54c5cf30b

object="stations" #phones or stations
phoneVersion=2
callResponse=$(curl --request GET http://configuration.us-east-1.inintca.com:80/configurations/v${phoneVersion}/organizations/$orgId/${object}?page=0)
currentPage=$(echo $callResponse | jsawk 'return this.totalPages')
desiredCode=200
increment=0
wasError=0
delayOperator=8
maxRetries=5

OUTPUT="desktop/bin/utilities/esManifestStationCacheChecker.txt"

if [ ! -f $OUTPUT ]; then
	touch $OUTPUT
else
	echo "" > $OUTPUT
fi

#echo "this is callResponse: "
#echo $callResponse

#meat of script

#get list of stations from /tenants/stations/
manifestOne=$(curl --request GET http://edge-config.us-east-1.inintca.com/edge-config/v1/edges/${edgeOne}/tenants/${orgId}/${object}/)
manifestOneArray=$(echo $manifestOne | jsawk -n 'forEach( IS, out($$.substr(-36, 36)) )')
manifestTwo=$(curl --request GET http://edge-config.us-east-1.inintca.com/edge-config/v1/edges/${edgeTwo}/tenants/${orgId}/${object}/)
manifestTwoArray=$(echo $manifestTwo | jsawk -n 'forEach( IS, out($$.substr(-36, 36)) )')
manifest=( ${manifestOneArray[@]} ${manifestTwoArray[@]} )
#manifestSpaces=${manifestOne}${manifestTwo}
#manifest="$(echo -e "${manifestSpaces}" | tr -d '[[:space:]]')"

#
#
# get station array from config by page. For each, pull edge config resource and see if station is listed in list of stations from /tenants/stations/
#
#

while [ "$currentPage" -gt 0 ]; do #while loop for each page

	echo ""
	echo "Getting current last page, page number "$currentPage"." #out status
	echo ""
	sleep 2
	callResponse=$(curl --request GET http://configuration.us-east-1.inintca.com:80/configurations/v${phoneVersion}/organizations/$orgId/${object}?page=$currentPage) #Get last page
	currentIdArray=$(echo $callResponse | jsawk 'return this.content' | jsawk 'return this.id') #Get station IDs only as JSON object
	bashIdArray=$(echo $currentIdArray | jsawk -n 'forEach( IS , out($$) )') #transform JSON into BASH-usable array

#	echo $bashIdArray

	#pause every $delayOperator GETs	
	if [[ "$currentPage" -ge "$delayOperator" ]]; then #don't divide by 0, see inner code
		((modValue="$currentPage" % "$delayOperator")) #sleep 1s every $delayOperator seconds
		if [[ "$modValue" -eq 0 ]]; then
			sleep 2
		fi
	fi

	arrayCount=0 #initialize counter

	#delete IDs in array from GET
	for i in $bashIdArray; do #do for done loop for each id on the current page
		
		((arrayCount=$arrayCount + 1))

		#pause every $delayOperator DELETEs	
		if [[ "$arrayCount" -ge "$delayOperator" ]]; then #don't divide by 0, see inner code
			((modValue="$arrayCount" % "$delayOperator")) #sleep 1s every $delayOperator seconds
			if [[ "$modValue" -eq 0 ]]; then
				sleep 1
			fi
		fi

		echo $i" -CHECKING RESOURCE-" #out status
		
		verificationCode=$(curl -I --silent --request GET http://edge-config.us-east-1.inintca.com/edge-config/v1/organizations/231e01e8-c97c-4335-87f1-753b0c3d2c24/edges/${edgeOne}/${object}/${i} | head -n 1 | cut -d$' ' -f2) #send query and capture response status code
		
		if [ "$verificationCode" -eq "$desiredCode"  ]; then
			echo $i" -FOUND ON EDGE "$edgeOne"-"
		elif [ "$verificationCode" -eq "404" ]; then
			verificationCode=$(curl -I --silent --request GET http://edge-config.us-east-1.inintca.com/edge-config/v1/organizations/231e01e8-c97c-4335-87f1-753b0c3d2c24/edges/${edgeTwo}/${object}/${i} | head -n 1 | cut -d$' ' -f2) #send query and capture response status code
			if [ "$verificationCode" -eq "$desiredCode" ]; then #increment if status code is correct
				echo $i" -FOUND ON EDGE "$edgeTwo"-"
			elif [ "$verificationCode" -eq "404" ]; then
				echo ""
				echo "------------------------- 404 INVESTIGATE: "$i" ---------------------------"
				echo ""
				echo $i" - URI fetches 404" >> $OUTPUT		
			else
				echo ""
				echo "ERROR: "$verificationCode
				echo ""

				#reset retryCount if there have been at least $delayOperator successes since last failure
				lastIncrement=$increment
				((sinceError=$increment - $lastIncrement))
				if [ "$sinceError" -ge "$delayOperator" ]; then
					retryCount=0
				fi

				((retryCount=$retryCount + 1))

				sleep $(($retryCount * 10))
				echo "Sleeping "$(($retryCount * 10))" seconds."

				if [ "$retryCount" -ge "$maxRetries" ]; then
					currentPage=0 #make while loop stop
				fi
				break #exit for do done loop
			fi		
		else
			echo ""
			echo "ERROR: "$verificationCode
			echo ""

			#reset retryCount if there have been at least $delayOperator successes since last failure
			lastIncrement=$increment
			((sinceError=$increment - $lastIncrement))
			if [ "$sinceError" -ge "$delayOperator" ]; then
				retryCount=0
			fi

			((retryCount=$retryCount + 1))

			sleep $(($retryCount * 10))
			echo "Sleeping "$(($retryCount * 10))" seconds."

			if [ "$retryCount" -ge "$maxRetries" ]; then
				currentPage=0 #make while loop stop
			fi
			break #exit for do done loop
		fi

		#test if this station is in the manifest
		echo $i" -CHECKING MANIFEST-"

		if `echo ${manifest[@]} | grep -q "$i"`; then
			echo $i" -FOUND IN MANIFEST-"
		else
			echo ""
			echo "------------------------- NOT IN MANIFEST: "$i" ---------------------------"
			echo ""
			echo $i" - NOT IN MANIFEST" >> $OUTPUT	
		fi

		((increment=$increment + 1)) #if successful, increment number of successful operations

	done

	((currentPage=$currentPage - 1)) #decrement while counter

done;

#Report final results to terminal
echo "There were "$increment" successful operations";

if [ "$increment" -eq 0 ]; then
	echo "There was an early failure. Make sure OpenVPN is connected."
fi