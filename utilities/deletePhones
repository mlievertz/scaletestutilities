#!/bin/bash

#NOTE ON DEPENDENCIES: relies on jsawk and having a bastion host connection to the VPC and ssh config so that http://localhost:8205 redirects to TCA config service.
#Get jsawk here: https://github.com/micha/jsawk#setup
#Follow the install/setup instructions as noted.
#Essentially jsawk needs Spidermonkey, which can be gotten using brew or macports, and some of the previous need xcode on mac, I think.
#jsawk is a bash script that you download, make executable, and either place in your path or in /bin or link from there.
#curl should come standard, so you probably already have it.

#some variables
orgId=9105eb71-1b8b-4b01-9a73-8fd04789ff33
object="phones" #phones or stations
phoneVersion=2
callResponse=$(curl --request GET http://configuration.us-east-1.inintca.com:80/configurations/v${phoneVersion}/organizations/$orgId/${object}?page=0)
currentPage=$(echo $callResponse | jsawk 'return this.totalPages')
desiredCode=200
increment=0
wasError=0
delayOperator=8
maxRetries=5

#echo "this is callResponse: "
#echo $callResponse

#meat of script
while [ "$currentPage" -gt 0 ]; do #while loop for each page

	echo "Getting current last page, page number "$currentPage"." #out status
	sleep 2
	callResponse=$(curl --request GET http://configuration.us-east-1.inintca.com:80/configurations/v${phoneVersion}/organizations/$orgId/${object}?page=$currentPage) #Get last page
	currentIdArray=$(echo $callResponse | jsawk 'return this.content' | jsawk 'return this.id') #Get station IDs only as JSON object
	bashIdArray=$(echo $currentIdArray | jsawk -n 'forEach( IS , out($$) )') #transform JSON into BASH-usable array

	#pause every $delayOperator GETs	
	if [[ "$currentPage" -ge "$delayOperator" ]]; then #don't divide by 0, see inner code
		((modValue="$currentPage" % "$delayOperator")) #sleep 1s every $delayOperator seconds
		if [[ "$modValue" -eq 0 ]]; then
			sleep 2
		fi
	fi

	arrayCount=0 #initialize counter

	#delete IDs in array from GET
	for i in $bashIdArray; do #do for done loop for each id on the current page
		
		((arrayCount=$arrayCount + 1))

		#pause every $delayOperator DELETEs	
		if [[ "$arrayCount" -ge "$delayOperator" ]]; then #don't divide by 0, see inner code
			((modValue="$arrayCount" % "$delayOperator")) #sleep 1s every $delayOperator seconds
			if [[ "$modValue" -eq 0 ]]; then
				sleep 1
			fi
		fi

		echo "Deleting "$i #out status
		
		verificationCode=$(curl -I --request DELETE http://configuration.us-east-1.inintca.com:80/configurations/v${phoneVersion}/organizations/$orgId/$object/$i | head -n 1 | cut -d$' ' -f2) #send delete and capture response status code
		
		if [ "$verificationCode" -eq "$desiredCode" ]; then #increment if status code is correct
			((increment=$increment + 1)) #if successful, increment number of successful operations
		else
			echo "There was an error: "$verificationCode

			#reset retryCount if there have been at least $delayOperator successes since last failure
			lastIncrement=$increment
			((sinceError=$increment - $lastIncrement))
			if [ "$sinceError" -ge "$delayOperator" ]; then
				retryCount=0
			fi

			((retryCount=$retryCount + 1))

			sleep $(($retryCount * 10))
			echo "Sleeping "$(($retryCount * 10))" seconds."

			if [ "$retryCount" -ge "$maxRetries" ]; then
				currentPage=0 #make while loop stop
			fi
			break #exit for do done loop

		fi

	done

	((currentPage=$currentPage - 1)) #decrement while counter

done;

#Report final results to terminal
echo "There were "$increment" successful operations";

if [ "$increment" -eq 0 ]; then
	echo "There was an early failure. Make sure bastion host for TCA is running."
fi