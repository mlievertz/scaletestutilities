#!/bin/bash

#NOTE ON DEPENDENCIES: relies on jsawk and having a bastion host connection to the VPC and ssh config so that http://localhost:8205 redirects to TCA config service.
#Get jsawk here: https://github.com/micha/jsawk#setup
#Follow the install/setup instructions as noted.
#Essentially jsawk needs Spidermonkey, which can be gotten using brew or macports, and some of the previous need xcode on mac, I think.
#jsawk is a bash script that you download, make executable, and either place in your path or in /bin or link from there.
#curl should come standard, so you probably already have it.

#some variables you might want to change
orgId=3585337c-0d2d-46ed-af99-5e89900c0041
desiredCode=200
first=640
last=999
phoneLabel="o3phone"
configObject="stations" #config object in api path
delayOperator=10
maxRetries=5
phonesVersion=2

#initializing some variables
cursor=$first
increment=0
wasError=0
emptyCheck=1
verificationCode=0

#meat of script
while [ "$cursor" -le "$last" ]; do #while loop for each phone

	echo "Getting record "$phoneLabel$cursor"." #out status

	callResponse=$(curl --request GET http://configuration.us-east-1.inintca.com:80/configurations/v${phonesVersion}/organizations/$orgId/$configObject?filters%5Blabel%5D=$phoneLabel$cursor) #Get current phone
	emptyCheck=$(echo $callResponse | jsawk 'return this.totalResults') #check if record exists
	echo "Found "$emptyCheck" objects."

	if [[ "$cursor" -ge "$delayOperator" ]]; then #don't divide by 0, see inner code
		((modValue="$cursor" % "$delayOperator")) #sleep 1s every $delayOperator seconds
		if [[ "$modValue" -eq 0 ]]; then
			sleep 1
		fi
	fi

	if [ "$emptyCheck" -gt 0 ]; then #only delete if record exists
		currentIdArray=$(echo $callResponse | jsawk 'return this.content' | jsawk 'return this.id') #Get IDs only as JSON object
		bashIdArray=$(echo $currentIdArray | jsawk -n 'forEach( IS , out($$) )') #transform JSON into BASH-usable array
		
		for i in $bashIdArray; do #do for done loop for each id on the current page - keeping structure for code reuse, not necessary for this purpose
			
			echo "Deleting "$i #out status

			verificationCode=$(curl -I --request DELETE http://configuration.us-east-1.inintca.com:80/configurations/v${phonesVersion}/organizations/$orgId/$configObject/$i | head -n 1 | cut -d$' ' -f2) #send delete and capture response status code
			
			if [ "$verificationCode" -eq "$desiredCode" ]; then #end script if response status code is wrong
				((increment=$increment + 1)) #if successful, increment number of successful operations
				((cursor=$cursor + 1)) #increment while counter
				retryCount=0 #reset current number of retries
			else
#				wasError=1 #flag for error

#				if [ "$wasError" -ne 0 ]; then
					echo "There was an error from the request for id "$i" which returned status code: "$verificationCode
#				fi

				((retryCount=$retryCount + 1))

				sleep $retryCount

				if [ "$retryCount" -ge "$maxRetries" ]; then
					((cursor=$last + 1)) #make while loop stop		
					break #exit for do done loop			
				fi
			fi
		done
	else
		echo "Record "$phoneLabel$cursor" does not exist." #let console know if record does not exist
		((cursor=$cursor + 1)) #increment while counter
	fi

done;

#Report final results to terminal
echo "There were "$increment" successful operations";

if [ "$increment" -eq 0 ]; then
	echo "There was an early failure. Make sure bastion host for TCA is running."
fi