#!/bin/bash

#NOTE ON DEPENDENCIES: relies on jsawk and having a bastion host connection to the VPC and ssh config so that http://localhost:8205 redirects to TCA config service.
#Get jsawk here: https://github.com/micha/jsawk#setup
#Follow the install/setup instructions as noted.
#Essentially jsawk needs Spidermonkey, which can be gotten using brew or macports, and some of the previous need xcode on mac, I think.
#jsawk is a bash script that you download, make executable, and either place in your path or in /bin or link from there.
#curl should come standard, so you probably already have it.

#some variables
orgIdArray=(b482f67c-46fb-428a-9f81-fa8ea889a8fb 8d304ed2-e808-44de-b7ce-9151c8d5ddd9 3585337c-0d2d-46ed-af99-5e89900c0041 601e881a-b93a-42a0-8d9d-cb7936b619af e9311867-88c7-4420-978f-bd12fb76ce42 04481277-9617-44f1-8aa8-214b50fd513c 6d46c343-edf8-4c93-8e5d-64beb33c554a 231e01e8-c97c-4335-87f1-753b0c3d2c24)
#
for orgId in ${orgIdArray[@]}; do 

	((orgCount=$orgCount + 1)) #if successful, increment number of successful operations	

	callResponse=$(curl --request GET http://configuration.us-east-1.inintca.com:80/configurations/v1/organizations/${orgId}/users)
	#echo $callResponse
	currentPage=$(echo $callResponse | jsawk 'return this.totalPages')
	desiredCode=200
	increment=0
	wasError=0
	delayOperator=8
	maxRetries=5

	#meat of script
	while [ "$currentPage" -gt 0 ]; do #while loop for each page

		echo "Getting current last page, page number "$currentPage"." #out status

		callResponse=$(curl --request GET http://configuration.us-east-1.inintca.com:80/configurations/v1/organizations/$orgId/users?page=$currentPage) #Get last page
		currentIdArray=$(echo $callResponse | jsawk 'return this.content' | jsawk 'return this.id') #Get station IDs only as JSON object
		bashIdArray=$(echo $currentIdArray | jsawk -n 'forEach( IS , out($$) )') #transform JSON into BASH-usable array
		
		for i in $bashIdArray; do #do for done loop for each id on the current page

		#pause every $delayOperator GETs	
		if [[ "$currentPage" -ge "$delayOperator" ]]; then #don't divide by 0, see inner code
			((modValue="$currentPage" % "$delayOperator")) #sleep 1s every $delayOperator seconds
			if [[ "$modValue" -eq 0 ]]; then
				sleep 2
			fi
		fi

			((arrayCount=$arrayCount + 1))

			#pause every $delayOperator DELETEs	
			if [[ "$arrayCount" -ge "$delayOperator" ]]; then #don't divide by 0, see inner code
				((modValue="$arrayCount" % "$delayOperator")) #sleep 1s every $delayOperator seconds
				if [[ "$modValue" -eq 0 ]]; then
					sleep 1
				fi
			fi
			
			echo "Deleting conversations for "$i #out status

			verificationCode=$(curl -I --request DELETE http://conversation.us-east-1.inintca.com:80/conversations/organizations/$orgId/users/$i/conversations | head -n 1 | cut -d$' ' -f2) #send delete and capture response status code
			
			if [ "$verificationCode" -eq "$desiredCode" ]; then #increment if status code is correct
				((increment=$increment + 1)) #if successful, increment number of successful operations
			else
				echo "There was an error: "$verificationCode

				#reset retryCount if there have been at least $delayOperator successes since last failure
				lastIncrement=$increment
				((sinceError=$increment - $lastIncrement))
				if [ "$sinceError" -ge "$delayOperator" ]; then
					retryCount=0
				fi

				((retryCount=$retryCount + 1))

				sleep $(($retryCount * 10))
				echo "Sleeping "$(($retryCount * 10))" seconds."

				if [ "$retryCount" -ge "$maxRetries" ]; then
					currentPage=0 #make while loop stop
				fi
				break #exit for do done loop

			fi

		done

		currentPage=$(($currentPage - 1)) #decrement while counter

	done;

	#Report final results to terminal
	echo "There were "$increment" successful operations completed for org "$orgId;
	echo "There were "$orgCount" orgs processed up to this point.";

done;

if [ "$increment" -eq 0 ]; then
	echo "There was an early failure. Make sure TCA openVPN is connected."
fi