#!/bin/bash



#NOTE ON DEPENDENCIES: relies on jsawk and having a bastion host connection to the VPC and ssh config so that http://localhost:8205 redirects to TCA config service.
#Get jsawk here: https://github.com/micha/jsawk#setup
#Follow the install/setup instructions as noted.
#Essentially jsawk needs Spidermonkey, which can be gotten using brew or macports (or whatever), and some of the previous need xcode on mac, I think.
#jsawk is a bash script that you download, make executable, and either place in your path or in /bin or link from there.
#curl should come standard, so you probably already have it.



#For this script, the idea is that you manually change some of the inputs here at the top to specify your org and time range, etc. 
#and then it will output a csv on your desktop that contains a lot of information about what analytics knows about your users' conversations
#for the specified time range.
#Currently, it outputs number of conversations and verifies for each:
#1) length
#2) presence of specified segments--you might need to adjust this for your call type
#	Currently it checks for Internal Dialing, Internal Connected, External Dialing, and External Connected
#3) presence of recording uuid
#
#the output csv makes errors pretty obvious and for each gives information that would be useful to verify/get more information



#some variables you might want to change

#Validation variables
recordingOn=true
validLengthTarget=630 #in seconds
validLengthVariation=5 #in seconds

validMinLength=$(($validLengthTarget - $validLengthVariation))
validMaxLength=$(($validLengthTarget + $validLengthVariation))

#Config inputs
orgId="e9311867-88c7-4420-978f-bd12fb76ce42"

configRequestObjectLabel="User" #prefix
configRequestCounterFirst="636" #first number to be used in counter
configRequestCounterLast="639" #last number to be used in counter
configRequestObjectSuffix="@TestMattLorg5.test" #suffix

#if you change script or config changes--probably don't touch these
configRequestObjectType="users" #config object in api path
configRequestFilter="username" #user has username, phone has label
configResponseDesiredStatusCode=200

#Interval to query for - note that this is in zulu time (our time is -5 hr in winter -4 hr in summer) - this needs to be a 30M interval - this uses IS0 8601 timestamp format
queryIntervalStartDate="2015-03-31"
queryIntervalStartHour="17"
queryIntervalStartMinute="30"
queryIntervalEndDate="2015-03-31"
queryIntervalEndHour="18"
queryIntervalEndMinute="30"

#number of segments to fetch at a time--tweaking may help or hurt efficiency
analyticsRequestBodyPageSize=50

#output file
overWrite=1 #value of 1 will overwrite if file already exists, other values will cause no overwriting (the script will append to the existing file)
outputDirectory="desktop"
outputType="csv"

#default output file path constructor
outputFile="${outputDirectory}/analyticsCheck_${configRequestObjectType}_${configRequestCounterFirst}-${configRequestCounterLast}_${queryIntervalStartDate}T${queryIntervalStartHour}${queryIntervalStartMinute}-${queryIntervalEndDate}T${queryIntervalEndHour}${queryIntervalEndMinute}.${outputType}"



#initializing stuff

analyticsRequestBodyInterval="${queryIntervalStartDate}T$queryIntervalStartHour:$queryIntervalStartMinute:00.000Z/${queryIntervalEndDate}T$queryIntervalEndHour:$queryIntervalEndMinute:00.000Z" #auto-generates from previous variables

cursor=$configRequestCounterFirst

if [ ! -f $outputFile ]; then #create output file if it doesn't exist
	touch $outputFile
else
	if [ "$overWrite" -eq 1 ]; then #if it exists and "overWrite" variable equals 1, then create a fresh file, destroying old one
		echo "User,UserId,No. Conv.,Conversations,Recordings,Problem Details" > $outputFile
	else #if overWrite doesn't equal 1, create new header for this run
		echo ""
		echo "User,UserId,No. Conv.,Conversations,Recordings,Problem Details" >> $outputFile
	fi
fi

#this stuff probably isn't necessary
configRequestActualResponseCode=0
successfulOperationsAccumulator=0
wasError=0
emptyCheck=0
totalConversationCounter=0



#Script
while [ "$cursor" -le "$configRequestCounterLast" ]; do #while loop for each config object to do operations on/with/for

	echo "Getting record for "$configRequestObjectLabel$cursor$configRequestObjectSuffix"." #out status

	configResponse=$(curl --request GET http://localhost:8205/configurations/v1/organizations/$orgId/$configRequestObjectType?filters%5B${configRequestFilter}%5D=$configRequestObjectLabel$cursor$configRequestObjectSuffix) #Get current object
	emptyCheck=$(echo $configResponse | jsawk 'return this.totalResults') #check if object exists

#	echo "Found "$emptyCheck" objects." #out status

	if [ "$emptyCheck" -gt 0 ]; then #only proceed if object exists
		configResponseCurrentIdArray=$(echo $configResponse | jsawk 'return this.content' | jsawk 'return this.id') #Get IDs only as JSON object
		configResponseBashIdArray=($(echo $configResponseCurrentIdArray | jsawk -n 'forEach( IS , out($$) )')) #transform JSON into BASH-usable array
		
		for perObjectOperationCounter in ${configResponseBashIdArray[@]}; do #loop for each id on the current page of results from config - keeping structure in case we want to pull multiple objects from config at a time - would have to remember cursor is currently incrementing for each object with first and last variables

#Operation for each perObjectOperationCounter

			echo $perObjectOperationCounter" - starting analytics queries for this user." #out status

			analyticsRequestBodyStartRecord=0 #record to start request at - initialize at 0 - I don't think this line is actually necessary
			analyticsRequestPagingAccumulator=0 #number of pages that have ALREADY been queried - initialize at 0
			perUserConversationCounter=0 #initialize at 0
			analyticsResponseCount=$analyticsRequestBodyPageSize #initialize while switch with 1
			perUserConversationList="Conversations"
			perUserRecordingList="Recordings"
			problemDetails="problemDetails"

			while [ "$analyticsResponseCount" -eq "$analyticsRequestBodyPageSize" ]; do #while loop paging through all conversations of a user

				#initial loop initialization activities
				unset analyticsResponseCount #just in case
				unset analyticsResponseBashIdArray #just in case

				analyticsRequestBodyStartRecord=$((${analyticsRequestBodyPageSize}*${analyticsRequestPagingAccumulator}))

				echo $perObjectOperationCounter" - Requesting page size: "$analyticsRequestBodyPageSize". Starting at:"$analyticsRequestBodyStartRecord"."

				#create analytics request
				analyticsRequestBodyBuilder="{\"qType\":\"discrete\",\"dataSource\":\"segments\",\"organizationId\":\"$orgId\",\"from\":$analyticsRequestBodyStartRecord,\"size\":$analyticsRequestBodyPageSize,\"order\":\"asc\",\"orderField\":\"eventStartTime\",\"filter\":{\"type\":\"dimensional\",\"dimensionName\":\"userId\",\"dimensionValue\":\"$perObjectOperationCounter\"},\"interval\":\"$analyticsRequestBodyInterval\"}"
#				echo "analyticsRequestBodyBuilder: "$analyticsRequestBodyBuilder
				analyticsRequestBody=$analyticsRequestBodyBuilder
#				echo "analyticsRequestBody: "$analyticsRequestBody

				#send request and get response
				analyticsResponse=$(curl -X POST -H "Content-Type: application/json" -H "Cache-Control: no-cache" -H "userAgent: MattLievertzVerificationScript" -d $analyticsRequestBody http://localhost:8207/api/v4/query) #get current object's first page
#				echo "analyticsResponse: "$analyticsResponse

				#test if response has a status indicating api call error
				analyticsResponseErrorStatus=$(echo $analyticsResponse | jsawk 'return this.status')
#				echo "analyticsResponseErrorStatus: "$analyticsResponseErrorStatus

				if [ -z "$analyticsResponseErrorStatus" ]; then #if no error, meaningless variable keep going
					:
				else #if there is any kind of error, output error and ask user what to do
					echo "Call failed. Details:"
					echo $analyticsResponse
					echo ""
					echo "Enter exactly [Y] to try again. Enter exactly [Next] to skip user. All other input will exit."
					read InputFromUser
					if [ $InputFromUser = "Y" ]; then
						x=x
					elif [ $InputFromUser = "Next" ]; then
						analyticsResponseCount=0
					else
						exit;
					fi
				fi

				#process call response into conversation count for user
				analyticsResponseCurrentIdArray=$(echo $analyticsResponse | jsawk 'return this.conversations' | jsawk 'return this.conversationId') #Get conversation IDs only as JSON object
#				echo "analyticsResponseCurrentIdArray: "$analyticsResponseCurrentIdArray
				analyticsResponseBashIdArray=($(echo $analyticsResponseCurrentIdArray | jsawk -n 'forEach( IS , out($$) )')) #transform JSON into BASH-usable array
#				echo "analyticsResponseBashIdArray: "$analyticsResponseBashIdArray
				if [ $analyticsResponseBashIdArray != "{}" ]; then
					analyticsResponseCount=${#analyticsResponseBashIdArray[@]} #count elements in the array
#					echo "analyticsResponseCount: "$analyticsResponseCount
#					echo ${analyticsResponseBashIdArray[@]}
					perUserConversationCounter=$(( $perUserConversationCounter + $analyticsResponseCount )) #add the conversations from this page to variable with count of all conversations for this user
#					echo "perUserConversationCounter: "$perUserConversationCounter

					#Validation portion of code
					for analyticsResponseConversation in ${analyticsResponseBashIdArray[@]}; do

						#incrementally build conversation list
						perUserConversationList=$perUserConversationList"_"$analyticsResponseConversation

						#validate existence of recordings
						if [ $recordingOn = true ]; then
							unset thisConversationRecording
							thisConversationRecording=$(echo $analyticsResponse | jsawk 'return this.conversations' | jsawk -v arg=$analyticsResponseConversation 'if (this.conversationId !== arg) return null' | jsawk -n 'out(this.segments)' | jsawk -n 'out(this.properties)' | jsawk -n 'out(this.value)')

							if [ "$thisConversationRecording" = "" ]; then
								problemDetails=$problemDetails","$analyticsResponseConversation" appears to have no recording uuid"
								problemDetailCounter=$(( $problemDetailCounter + 1 ))
								echo $analyticsResponseConversation" appears to have no recording uuid."
							fi
							perUserRecordingList=$perUserRecordingList"_"$thisConversationRecording
						fi

						#validate that conversation has segments for "Internal" and "External" "participantType" and "Dialing" and "Interact" "segmentType" and "Internal" "Interact" segment has "howEnded" "Disconnect"
						unset thisConversationExternalDialing
						thisConversationExternalDialing=$(echo $analyticsResponse | jsawk 'return this.conversations' | jsawk -v arg=$analyticsResponseConversation 'if (this.conversationId !== arg) return null' | jsawk -n 'out(this.segments)' | jsawk 'if (this.participantType == "External" && this.segmentType == "Dialing") {return this.segmentId;} else {return null}' | jsawk -n 'out(this)')

						unset thisConversationExternalInteract
						thisConversationExternalInteract=$(echo $analyticsResponse | jsawk 'return this.conversations' | jsawk -v arg=$analyticsResponseConversation 'if (this.conversationId !== arg) return null' | jsawk -n 'out(this.segments)' | jsawk 'if (this.participantType == "External" && this.segmentType == "Interact") {return this.segmentId;} else {return null}' | jsawk -n 'out(this)')
						
						unset thisConversationInternalDialing
						thisConversationInternalDialing=$(echo $analyticsResponse | jsawk 'return this.conversations' | jsawk -v arg=$analyticsResponseConversation 'if (this.conversationId !== arg) return null' | jsawk -n 'out(this.segments)' | jsawk 'if (this.participantType == "Internal" && this.segmentType == "Dialing") {return this.segmentId;} else {return null}' | jsawk -n 'out(this)')
						
						unset thisConversationInternalInteract
						thisConversationInternalInteract=$(echo $analyticsResponse | jsawk 'return this.conversations' | jsawk -v arg=$analyticsResponseConversation 'if (this.conversationId !== arg) return null' | jsawk -n 'out(this.segments)' | jsawk 'if (this.participantType == "Internal" && this.segmentType == "Interact") {return this.segmentId;} else {return null}' | jsawk -n 'out(this)')
					
						if [ "$thisConversationExternalDialing" = "" ] || [ "$thisConversationExternalInteract" = "" ] || [ "$thisConversationInternalDialing" = "" ] || [ "$thisConversationInternalInteract" = "" ]; then
							problemDetails=$problemDetails","$analyticsResponseConversation" appears to be missing a critical segment. External Dialing: "$thisConversationExternalDialing" External Interact: "$thisConversationExternalInteract" Internal Dialing: "$thisConversationInternalDialing" Internal Interact: "$thisConversationInternalInteract
							problemDetailCounter=$(( $problemDetailCounter + 1 ))
							echo $analyticsResponseConversation" appears to be missing a critical segment."
						else
#							echo "Conversation "$analyticsResponseConversation" appears to have all critical segments."
#							echo "External Dialing: "$thisConversationExternalDialing
#							echo "External Interact: "$thisConversationExternalInteract
#							echo "Internal Dialing: "$thisConversationInternalDialing
#							echo "Internal Interact: "$thisConversationInternalInteract
							:
						fi

						#validate that conversation lasted within an acceptable time range
						unset conversationStartTime conversationStartValue year month day hour min sec mS tZ
						conversationStartTime=$(echo $analyticsResponse | jsawk 'return this.conversations' | jsawk -v arg=$analyticsResponseConversation 'if (this.conversationId !== arg) return null' | jsawk -n 'out(this.segments)' | jsawk 'if (this.participantType == "Internal" && this.segmentType == "Interact") {return this.segmentStart;} else {return null}' | jsawk -n 'out(this)')
						read year month day hour min sec mS tZ <<< $(echo $conversationStartTime | sed 's/[^0-9]/ /g')
						conversationStartValue=$(date -j -f "%Y %m %d %H %M %S" "$year $month $day $hour $min $sec" "+%s")

						unset conversationEndTime conversationEndValue year month day hour min sec mS tZ
						conversationEndTime=$(echo $analyticsResponse | jsawk 'return this.conversations' | jsawk -v arg=$analyticsResponseConversation 'if (this.conversationId !== arg) return null' | jsawk -n 'out(this.segments)' | jsawk 'if (this.participantType == "Internal" && this.segmentType == "Interact") {return this.segmentEnd;} else {return null}' | jsawk -n 'out(this)')
						read year month day hour min sec mS tZ <<< $(echo $conversationEndTime | sed 's/[^0-9]/ /g')
						conversationEndValue=$(date -j -f "%Y %m %d %H %M %S" "$year $month $day $hour $min $sec" "+%s")

						if [ "$conversationEndValue" ] && [ "$conversationStartValue" ]; then
							conversationLength=$(( $conversationEndValue - $conversationStartValue ))
						else
							problemDetails=$problemDetails",Conversation "$analyticsResponseConversation" length was misformatted"

							problemDetailCounter=$(( $problemDetailCounter + 1 ))
							echo $perObjectOperationCounter" - "$analyticsResponseConversation" appears to have had a misformatted timestamp."
						fi

						if [ "$conversationLength" -lt "$validMaxLength" ] && [ "$conversationLength" -gt "$validMinLength" ]; then
							echo $perObjectOperationCounter" - "$analyticsResponseConversation" appears to have been within the specified length range."
						else
							problemDetails=$problemDetails",Conversation "$analyticsResponseConversation" length was: "$conversationLength" (not in "$validLengthTarget" plus or minus "$validLengthVariation")"

							problemDetailCounter=$(( $problemDetailCounter + 1 ))
							echo $perObjectOperationCounter" - "$analyticsResponseConversation" appears to have been too short or too long."
						fi

					done;

				else
					analyticsResponseCount=0
					problemDetails=$problemDetails","$perObjectOperationCounter" has no conversations."
					problemDetailCounter=$(( "$problemDetailCounter" + 1 ))
					echo $perObjectOperationCounter" - has no conversations."

				fi

				analyticsRequestPagingAccumulator=$(( $analyticsRequestPagingAccumulator + 1 )) #increment paging accumulator
				echo $perObjectOperationCounter" - loop number "$analyticsRequestPagingAccumulator" complete. Found "$analyticsResponseCount" conversations." #out status

			done;

			#record results for user and some incrementation
			successfulOperationsAccumulator=$(( $successfulOperationsAccumulator + 1 ))

			echo ${configRequestObjectLabel}${cursor}","$perObjectOperationCounter","$perUserConversationCounter","$perUserConversationList","$perUserRecordingList","$problemDetails >> $outputFile
			totalConversationCounter=$(( $totalConversationCounter + $perUserConversationCounter )) #increment count of total conversations - note this will double count station-to-station calls if both users in selected users

			echo ""
			echo $perObjectOperationCounter" - "${configRequestObjectLabel}${cursor}
			echo $perObjectOperationCounter" - "$perUserConversationCounter" Conversations found"
			echo $perObjectOperationCounter" - Conversations: "$perUserConversationList
			echo $perObjectOperationCounter" - Recordings: "$perUserRecordingList
			echo $perObjectOperationCounter" - "$problemDetails
			echo ""
			echo "TOTAL currently at "$totalConversationCounter" after querying for "${configRequestObjectLabel}${cursor}"."
			echo "Currently at "$problemDetailCounter" total possible error cases after querying for "${configRequestObjectLabel}${cursor}"."
			echo ""

		done;

	else
		echo ">>>>>> Record for "$configRequestObjectLabel$cursor" does not exist." #let console know if record does not exist
	fi

	cursor=$(($cursor + 1)) #increment while counter

done;

echo "TOTAL,"$totalConversationCounter >> $outputFile
echo "TOTAL,"$totalConversationCounter" written to file."

#Report final results to terminal
echo "There were "$successfulOperationsAccumulator" successful objects queried for giving a total of "$totalConversationCounter" conversations in analytics";

if [ "$successfulOperationsAccumulator" -eq 0 ]; then
	echo "There was an early failure. Make sure bastion host for TCA is running."
fi

if [ "$wasError" -ne 0 ]; then
	echo "There was an error from the request for id "$i" which returned status code: "$statusCode
fi

exit;