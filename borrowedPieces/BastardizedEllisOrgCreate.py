#!/usr/bin/env python

import argparse
import base64
import getpass
import json
import requests
import subprocess
import time

__author__ = 'evanellis'


# Parse command line args:
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('org_name', help='name of the org that will be created.'
                                         'If you are creating more than one, a number will be appended onto each')
    parser.add_argument('--username', help='email of ININ user to boostrap the process. '
                                           'will prompt if none provided', default=None)
    parser.add_argument('--password', help='password of ININ user to boostrap the process. '
                                           'will secure prompt if none provided', default=None)
    parser.add_argument('--new_name', help='username for new admin(s)', default='admin')
    parser.add_argument('--new_pass', help='password for new admin(s) and user(s)', default='test1234')
    parser.add_argument('--encoded_client', help='client used to log in if a new one is not created. '
                                                 '(client id:secret in base64 encoding. is overridden by --new_client)'
                                                 '', default='YzA1YjNjN2YtMmU4NC00ZjhhLWJmYzEtNzU5YjJlNzBmMGM2OjlEczZaaV82eWJ5UzcyclNsQmpCSGt5QV9jdGVRcjNuZS0wcXBOakVENkk=')
    parser.add_argument('--new_client', help='creates a new OAuth client and logs in with it '
                                             '(will be created in the entered users org)'
                                             'REQUIRES VPN to env', action='store_true')
    parser.add_argument('--write_path', help='directory to write output to (this app can create a lot of files)'
                                             '', default='/tmp/org_outputs/')
    parser.add_argument('--env', help='the environment to create the org in.', default='tca')
    parser.add_argument('--phone_count', help='the number of phones to create in each org', default=None)
    parser.add_argument('--org_count', help='number of orgs to be created', default=1)
    parser.add_argument('--orgs_per_bis', help='number of orgs in each CSV '
                                               '(MUST keep total users/CSV under 10k)', default=1)
    parser.add_argument('--user_count', help='number of users in each org', default=1000)
    parser.add_argument('--queue_count', help='number of queues each user will be in', default=None)
    parser.add_argument('--queue_size', help='the size of each queue '
                                             '(MUST divide evenly into user count)', default=None)
    parser.add_argument('--verbose', help='if enabled, writes success messages to log', action='store_true')
    return parser.parse_args()


# General purpose run command:
def run(cmd, hide_command=False, raise_on_failure=True):
    if hide_command is False:
        print('cmd: ' + str(cmd))
    output = None
    returncode = None
    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
        output = p.communicate()[0]
        returncode = p.returncode
    except Exception, ex:
        print('***Error in command: ' + str(cmd))
        print(ex.message)
    if returncode != 0 and raise_on_failure == True:
        print('***Error in command and raiseOnFailure is True so exiting. CMD:\n' + cmd)
        print('This is the output from that command, if any:\n' + output)
        raise Exception("Command_Error")
    return output, returncode


class REST(object):
    """Creates compound REST calls and stores session info."""
    def __init__(self, session_name):
        self.session_name = session_name

    # This object can hold your cookies!
    s = requests.Session()

    # "app_json" transforms the request body to json
    # GET Formatting
    def get(self, url):
        result = self.s.get(url)
        result.raise_for_status()
        return result

    # POST Formatting
    def post(self, url,  data, headers, app_json=False):
        if app_json is False:
            result = self.s.post(url, data=data, headers=headers)
            result.raise_for_status()
            return result.json()
        elif app_json is True:
            result = self.s.post(url, data=json.dumps(data), headers=headers)
            result.raise_for_status()
            return result.json()

    # PUT Formatting
    def put(self, url,  data, headers, app_json=False):
        if app_json is False:
            result = self.s.put(url, data=data, headers=headers)
            result.raise_for_status()
            return result.json()
        elif app_json is True:
            result = self.s.put(url, data=json.dumps(data), headers=headers)
            result.raise_for_status()
            return result.json()

    # DELETE Formatting
    def delete(self, url,  data, headers, app_json=False):
        if app_json is False:
            result = self.s.delete(url, data=data, headers=headers)
            result.raise_for_status()
            return result.json()
        elif app_json is True:
            result = self.s.delete(url, data=json.dumps(data), headers=headers)
            result.raise_for_status()
            return result.json()


class OAuthClient(object):
    """Logs in a user with OAuth2 and creates new clients if needed"""
    def __init__(self, org_id, session, env):
        self.env = env
        self.org_id = org_id
        self.session = session

    # Creates http headers
    @staticmethod
    def make_headers(encoded_client=None):
        if encoded_client is not None:
            headers = {'Authorization': 'Basic ' + encoded_client, 'Content-Type': 'application/x-www-form-urlencoded'}
        else:
            headers = {'content-type': 'application/json'}
        return headers

    # Creates a new OAuth2 client if one is needed
    def new_client(self):
        url = 'http://auth-api.us-east-1.inin' + self.env + '.com/v1/clients'
        data = {'organizationId': self.org_id,
                'name': 'Test Client',
                'description': 'Test Client',
                'scoped': True,
                'authorizedGrantTypes': ['code', 'token', 'password'],
                'registeredRedirectUri': ['http://localhost:8085/oauth/callback'],
                'accessTokenValiditySeconds': 14400,
                'refreshTokenValiditySeconds': 7200,
                'autoApprove': True
                }
        headers = self.make_headers()
        result = self.session.post(url, data, headers, app_json=True)
        return result

    # Gets an existing OAuth client for the org
    def id_get(self):
        url = 'http://auth-api.us-east-1.inin' + self.env + '.com/v1/clients?organizationId=' + self.org_id
        result = self.session.get(url)
        client_id = json.loads(result.text)[0]['id']
        url2 = 'http://auth-api.us-east-1.inin' + self.env + '.com/v1/clients/' + client_id
        result2 = self.session.get(url2)
        secret = json.loads(result2.text)['secret']
        return client_id, secret

    # Logs in with an existing client
    def client_login(self, encoded_client, username, password):
        url = 'https://login.inin' + self.env + '.com/token'
        headers = self.make_headers(encoded_client)
        data = 'grant_type=password&username=' + username + '&password=' + password
        result = self.session.post(url, data, headers)
        return result


class WritePath(object):
    """Stores write paths for CSVs and logs."""
    def __init__(self, write_path):
        self.write_path = write_path

    def csv_name(self):
        return '' + self.write_path + 'BIS_'

    def log_name(self):
        return '' + self.write_path + 'log.txt'


class OrgData(object):
    """Creates/holds org data based on inputs for use in other functions."""
    def __init__(self, env, org_name, org_count, orgs_per_bis, phone_count, user_count, queue_count, queue_size):
        self.env = env
        self.org_name = org_name
        self.org_count = int(org_count)
        self.orgs_per_bis = int(orgs_per_bis)
        self.phone_count = int(phone_count)
        self.user_count = int(user_count)
        self.queue_count = int(queue_count)
        self.queue_size = int(queue_size)

    # Confirms that data provided in args is valid for this utility
    def usability_check(self):
        if self.queue_size is not None:
            if self.user_count % self.queue_size != 0:
                print(' :     ***Error: Room size does not divide evenly into the number of users.'
                      ' This will cause bleed-over into the next org and is NOT allowed.')
                raise Exception('Invalid room size')
        if self.user_count is not None:
            if self.user_count * self.orgs_per_bis > 10000:
                print(' :     ***Error: The number of users in a single CSV is over 10,000; '
                      'this is known to cause BIS instability. '
                    'Please reduce the number of users per CSV and try again.')
                raise Exception('CSV too large')
        else:
            if args.verbose:
                print ' :     All data passed into the application appears to be valid'

    # Generates list of orgs with numbers appended to the end and the list to be used for BIS CSV rotation
    def org_list(self):
        split_list = [x for x in range(1, self.org_count)]
        full_list = []
        for count in range(self.org_count):
            full_list.append(self.org_name + str(count + 1))
        return full_list, split_list[::self.orgs_per_bis]


class OrgSetup(object):
    """Generates the org and all requested attributes."""
    def __init__(self, session, token, env):
        self.session = session
        self.env = env
        self.token = token

    # Creates http headers
    @staticmethod
    def make_headers(token=None):
        headers = {'content-type': 'application/json', 'Accept': 'application/json'}
        if token is not None:
            headers['Authorization'] = 'bearer ' + token
        return headers

    # Creates a username/email address
    @staticmethod
    def make_user_name(name, org_name):
        return name + '@' + org_name + '.test'

    # Creates an org using an authenticated user
    def create_org(self, org_name, username, password):
        url = 'https://public-api.us-east-1.inin' + self.env + '.com/api/v1/configuration/organizations'
        headers = self.make_headers(token=self.token)
        admin_name = self.make_user_name(username, org_name)
        data = {'name': org_name,
                'thirdPartyOrgName': org_name,
                'adminUsername': admin_name,
                'adminPassword': password,
                'domain': org_name + 'domain'
                }
        result = self.session.post(url, data, headers, app_json=True)
        print result
        return result['id'], admin_name


# Gets an auth token for a user via OAuth username/password grant
def full_login(rest_session):
    # If no username and/or password are provided in the arguments, prompts for them here
    # (this is more secure, but less automation friendly)
    if args.username is not None:
        username = args.username
    else:
        username = raw_input('Username:')

    if args.password is not None:
        password = args.password
    else:
        password = getpass.getpass()

    if args.new_client:
        admin_object = rest_session.get('http://configuration.us-east-1.inin' + args.env + '.com/configurations/v1/users?username=' + username)
        org = json.loads(admin_object.text)['content'][0]['organizationId']
    else:
        org = "null"
    auth = OAuthClient(org, rest_session, args.env)

    if args.new_client:
        auth.new_client()
        client_id, client_secret = auth.id_get()
        encoded_client = base64.b64encode(client_id + ':' + client_secret)
        token_response = auth.client_login(encoded_client, username, password)
        token = token_response['access_token']
    else:
        encoded_client = args.encoded_client
        token_response = auth.client_login(encoded_client, username, password)
        token = token_response['access_token']

    return token, encoded_client


# Creates the org(s) and all requested contents
def generate_orgs(): #rest_session, token
    #org_gen = OrgSetup(rest_session, token, org_info.env)
    time.sleep(10)
    bis_j = open(args.write_path + '/bisObject.json', 'w')
    bis = {}
    # Start constructing the JSON here:
    bis["type"] = "Simulation"
    bis["bISVersion"] = "3.1.5714.33495"
    bis["stopOnInteractionFailure"] = False
    bis["pureCloudMaxLogoutFrequencyHz"] = 16
    bis["globalICServers"] = [{"serverType": "IC", "iCServer": "localhost"}]
    bis["schedules"] = []

    for org in range(org_info.org_count):
        bis["schedules"].append(org)

    for org in range(org_info.org_count):
        #org_id, admin_name = org_gen.create_org(org_info.org_name + str(org + 1), args.new_name, args.new_pass)
        # Periodic Burst Schedule creation
        bis["schedules"][org] = {}
        bis["schedules"][org]["type"] = "PeriodicBurstSchedule"
        bis["schedules"][org]["maxInteractions"] = 1
        bis["schedules"][org]["numInteractions"] = 1
        bis["schedules"][org]["burstIntervalMilliseconds"] = 1000
        # Scenario creation
        bis["schedules"][org]["scenario"] = {}
        bis["schedules"][org]["scenario"]["type"] = "PureCloudUserScenario"
        bis["schedules"][org]["scenario"]["scenarioName"] = "Org" + str(org + 1)
        bis["schedules"][org]["scenario"]["injectActionsAtRuntime"] = False
        bis["schedules"][org]["scenario"]["disconnectConversationsAtConclusion"] = False
        bis["schedules"][org]["scenario"]["preventMultipleInteractionsOfSameUser"] = False
        bis["schedules"][org]["scenario"]["users"] = [{"username": "admin@" + org_info.org_name + str(org + 1) + ".test", "password": args.new_pass}]
        bis["schedules"][org]["scenario"]["pureCloudEnvironment"] = {"preconfiguredEnvironment": str(org_info.env).upper()}
        bis["schedules"][org]["scenario"]["sessionOptions"] = {"preconfiguredSettings": "Communicate"}
        # Action creation
        bis["schedules"][org]["scenario"]["actions"] = []
        action_count = 0
        # if org_info.user_count is not None:
        #     action_count += 1
        # if org_info.phone_count is not None:
        #     action_count += 1
        if org_info.queue_count is not None:
            action_count += (org_info.queue_count * 1)
        for count in range(action_count):
            bis["schedules"][org]["scenario"]["actions"].append(count)
        set_count = 0
        # Users
        # if org_info.user_count is not None:
        #     bis["schedules"][org]["scenario"]["actions"][set_count] = {}
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["type"] = "CreateOrgSpanUsersAction"
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["concurrentRequestCount"] = 2
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["lastNameFirst"] = True
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["newUserCount"] = org_info.user_count
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["newUserPassword"] = args.new_pass
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["emailPrefix"] = "User"
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["emailDomain"] = org_info.org_name + str(org + 1) + ".test"
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["emailAddressStartingIndex"] = 0
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["createWithAdminRights"] = False
        #     if int(org + 1) in range(0, 9):
        #         bis["schedules"][org]["scenario"]["actions"][set_count]["basePhoneNumber"] = "131750" + str(org + 1) + "0000"
        #     if int(org + 1) in range(10, 99):
        #         bis["schedules"][org]["scenario"]["actions"][set_count]["basePhoneNumber"] = "13175" + str(org + 1) + "0000"
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["shouldDeleteUsersAfterInteraction"] = False
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["distributionQueues"] = []
        #     set_count += 1
        # Queues
        if org_info.queue_count is not None:
        #     for queue in range(org_info.queue_count):
        #         bis["schedules"][org]["scenario"]["actions"][set_count] = {}
        #         bis["schedules"][org]["scenario"]["actions"][set_count]["type"] = "CreateDeletePureMatchQueuesAction"
        #         bis["schedules"][org]["scenario"]["actions"][set_count]["queueOperation"] = "Create"
        #         bis["schedules"][org]["scenario"]["actions"][set_count]["requestDelayMilliseconds"] = 0
        #         bis["schedules"][org]["scenario"]["actions"][set_count]["failOnCreateDups"] = False
        #         bis["schedules"][org]["scenario"]["actions"][set_count]["deleteDups"] = False
        #         bis["schedules"][org]["scenario"]["actions"][set_count]["queueNames"] = ["ScaleQueue" + str(queue + 1)]
        #         set_count += 1
        # Queue Membership
            for queue in range(org_info.queue_count):
                bis["schedules"][org]["scenario"]["actions"][set_count] = {}
                bis["schedules"][org]["scenario"]["actions"][set_count]["type"] = "ModifyQueueMembershipAction"
                bis["schedules"][org]["scenario"]["actions"][set_count]["queueName"] = "ScaleQueue" + str(queue + 1)
                bis["schedules"][org]["scenario"]["actions"][set_count]["changeType"] = "AddIfNotPresent"
                email_list = []
                for user in range(org_info.user_count):
                    email_list.append("User" + str(user + 1) + "@" + org_info.org_name + str(org + 1) + ".test")
                bis["schedules"][org]["scenario"]["actions"][set_count]["userEmailAddresses"] = email_list
                set_count += 1
        # Phones
        # if org_info.phone_count is not None:
        #     bis["schedules"][org]["scenario"]["actions"][set_count] = {}
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["type"] = "CreatePureCloudPhonesAction"
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["concurrentRequestCount"] = 2
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["newPhoneCount"] = org_info.user_count
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["phoneNamePrefix"] = "load" + str(org + 1) + "Phone"
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["edgeGroup"] = "TestEdgeGroup"
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["startingIndex"] = 0
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["baseSettings"] = "AudioCodes 420HD Template"
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["site"] = "TestSite"
        #     bis["schedules"][org]["scenario"]["actions"][set_count]["shouldDeletePhonesAfterInteraction"] = False

    bis_j.write(json.dumps(bis, indent=4))
    bis_j.close()


# Generate CSV for easy copy/paste into BIS (all emails will end in ".test")
def user_list_ordered():
    lists = [250, 350, 450, 500, 640, 750, 1000, 1250, 1500, 1750, 2000]
    for amount in lists:
        bis_counter = 0
        bis_count = (org_info.org_count / org_info.orgs_per_bis)
        org_counter = 1
        while bis_counter < bis_count:
            u = open(directory.csv_name() + str(bis_counter + 1) + 'num' + str(amount) + 'users.csv', 'w')
            p = open(directory.csv_name() + str(bis_counter + 1) + 'num' + str(amount) + 'phones_0.txt', 'w')
            q = open(directory.csv_name() + str(bis_counter + 1) + 'num' + str(amount) + 'phones.txt', 'w')
            for org in range(0,11):
                user_counter = 1
                while user_counter <= amount:
                    u.write('User' + str(user_counter) + '@' + str(args.org_name) + str(bis_counter + 1) + '.test,' + args.new_pass + '\n')
                    user_counter += 1
                if org_info.phone_count is not None:
                    phone_counter = 1
                    while phone_counter <= amount:
                        p.write('load' + str(bis_counter + 1) + 'Phone' + str(phone_counter) + '_0' + '\n')
                        phone_counter += 1
                if org_info.phone_count is not None:
                    phone_counter = 1
                    while phone_counter <= amount:
                        q.write('load' + str(bis_counter + 1) + 'Phone' + str(phone_counter) + '\n')
                        phone_counter += 1
                org_counter += 1
            u.close()
            p.close()
            q.close()
            bis_counter += 1


# Application execution
if __name__ == '__main__':
    args = parse_args()

    directory = WritePath(args.write_path)

    org_info = OrgData(args.env, args.org_name, args.org_count, args.orgs_per_bis, args.phone_count, args.user_count, args.queue_count, args.queue_size)
    org_info.usability_check()

    cmd = 'mkdir ' + args.write_path
    run(cmd)
    cmd = 'touch ' + args.write_path + '/bisObject.json'
    run(cmd)

    if args.user_count is not None:
        user_list_ordered()

    #bootstrap_session = REST('main_session')

    #auth_token, oauth_client = full_login(bootstrap_session)

    generate_orgs() #bootstrap_session, auth_token