__author__ = 'ben.scott'

import requests
import json


class PublicApi:

    def __init__(self, base_url):
        super().__init__()
        self.base_url = base_url
        self.cookies = {}

    def set_password(self, email, password):
        return requests.post(self.base_url + 'directory/api/v1/admin/setPassword',
                             data=json.dumps({'email': email, 'password': password}),
                             headers=self.cookies)

    def login(self, email, password):
        response = requests.post(self.base_url + 'api/v2/login',
                                 data=json.dumps({'email': email, 'password': password}))

        if response.status_code != requests.codes.ok:
            return response

        for cookie in response.headers['Set-Cookie'].split(', '):
            cookie = cookie.split(';')[0]
            value = cookie.split('=')
            self.cookies[value[0]] = value[1]

        return response

    def logout(self):
        response = requests.get(self.base_url + 'api/v1/logout',
                                headers=self.cookies)
        return response